#Directories
SRC_DIR := src
INC_DIR := include
OBJ_DIR := obj
DEP_DIR := .dep
LIB_DIR := lib
BIN_DIR := bin
BUILD_DIR := build
RELEASE_DIR := $(BUILD_DIR)/release
DEBUG_DIR := $(BUILD_DIR)/debug

#Compiler flags
CXXFLAGS = -std=c++20 -Wall -Wextra -Wpedantic
CPPFLAGS =
LDLIBS   =
LDFLAGS  =

#Build rules
COMP     = $(CXX) $(DEPFLAGS) $(CXXFLAGS) $(CPPFLAGS) -c $< -o $@
POSTCOMP = mv -f $(dir $@)/$(DEP_DIR)/$*.Td $(dir $@)/$(DEP_DIR)/$*.d && touch $@
LINK     = $(CXX) $^ $(LDFLAGS) $(LDLIBS) -o $@
ARCH     = $(AR) -rc $@ $^
DEPFLAGS = -MT $@ -MMD -MP -MF $(dir $@)/$(DEP_DIR)/$*.Td

include Rules.mk
