#include "GridLayoutStrategyHelpers.hpp"
#include <ranges>

namespace ncursesplus::layout
{
CellAvailabilityMatrix registerCells(CellAvailabilityMatrix&& cellAvailabilityMatrix, GridPosition gridPosition)
{
	auto newCellAvailabilityMatrix{std::move(cellAvailabilityMatrix)};

	auto [rowStart, rowEnd, colStart, colEnd]{gridPosition};

	for (auto row{rowStart}; row <= rowEnd; row++)
		for (auto col{colStart}; col <= colEnd; col++)
			newCellAvailabilityMatrix[row][col] = true;

	return newCellAvailabilityMatrix;
}

GridPosition computeVerticalGridPosition(const CellAvailabilityMatrix& cellAvailabilityMatrix)
{
	unsigned int numOfCols(cellAvailabilityMatrix.front().size());
	unsigned int numOfRows(cellAvailabilityMatrix.size());

	for (auto col: std::views::iota(0u, numOfCols))
		for (auto row: std::views::iota(0u, numOfRows))
			if (!cellAvailabilityMatrix[row][col])
				return {row, numOfRows - 1, col, col};

	return {};
}

GridPosition computeHorizontalGridPosition(const CellAvailabilityMatrix& cellAvailabilityMatrix)
{
	unsigned int numOfCols(cellAvailabilityMatrix.front().size());
	unsigned int numOfRows(cellAvailabilityMatrix.size());

	for (auto row: std::views::iota(0u, numOfRows))
		for (auto col: std::views::iota(0u, numOfCols))
			if (!cellAvailabilityMatrix[row][col])
				return {row, row, col, numOfCols - 1};

	return {};
}

GridPosition computeSingleGridPosition(const CellAvailabilityMatrix& cellAvailabilityMatrix)
{
	unsigned int numOfCols(cellAvailabilityMatrix.front().size());
	unsigned int numOfRows(cellAvailabilityMatrix.size());

	for (auto row: std::views::iota(0u, numOfRows))
		for (auto col: std::views::iota(0u, numOfCols))
			if (!cellAvailabilityMatrix[row][col])
				return {row, row, col, col};

	return {};
}
}
