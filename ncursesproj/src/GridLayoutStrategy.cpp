#include "GridLayoutStrategy.hpp"

#include "Point.hpp"
#include "Size.hpp"

namespace ncursesplus::layout
{
using namespace widget;

widget::Size GridLayoutStrategy::computeSize(GridPosition gridPosition) const
{
	auto [rowStart, rowEnd, colStart, colEnd] = gridPosition;

	auto parentSize{getParentSize()};

	auto parentHeight = parentSize.getHeight();
	auto parentWidth = parentSize.getWidth();

	auto cellHeight = parentHeight / getNumOfRows();
	auto cellWidth = parentWidth / getNumOfCols();

	Height height{cellHeight * (rowEnd - rowStart + 1)};
	Width width{cellWidth * (colEnd - colStart + 1)};

	return Size{height, width};
}

widget::Point GridLayoutStrategy::computePoint(GridPosition gridPosition) const
{
	auto parentSize{getParentSize()};

	auto parentHeight = parentSize.getHeight();
	auto parentWidth = parentSize.getWidth();

	auto cellHeight = parentHeight / getNumOfRows();
	auto cellWidth = parentWidth / getNumOfCols();

	auto pos = gridPosition.colStart + gridPosition.rowStart * getNumOfCols();

	auto parentPoint{getParentPoint()};

	auto beginY = pos / getNumOfCols() * cellHeight + parentPoint.getY();
	auto beginX = pos % getNumOfCols() * cellWidth + parentPoint.getX();

	return widget::Point{Y{beginY}, X{beginX}};
}
}
