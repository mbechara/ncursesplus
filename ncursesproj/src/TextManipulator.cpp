#include "TextManipulator.hpp"
#include "Size.hpp"
#include <algorithm>

namespace ncursesplus::widget
{
template class BaseTextManipulator<TextManipulator<ScrollMechanism::WITHOUT_SCROLL>>;
template class BaseTextManipulator<TextManipulator<ScrollMechanism::WITH_SCROLL>>;

template<typename TextManipulator>
BaseTextManipulator<TextManipulator>::BaseTextManipulator(Size textBoxSize, TextBoxProperties&& textBoxProperties)
	 : m_textBoxHeight{textBoxSize.getHeight()},
		m_textBoxWidth{textBoxSize.getWidth()},
		m_textBoxProperties{textBoxProperties}
{}

template<typename TextManipulator>
void BaseTextManipulator<TextManipulator>::setCursorPos(Point cursorPos)
{
	m_cursorPos = cursorPos;
}

template<typename TextManipulator>
Point BaseTextManipulator<TextManipulator>::getCursorPos() const
{
	return m_cursorPos;
}

template<typename TextManipulator>
std::string BaseTextManipulator<TextManipulator>::getFullText() const
{
	std::string text;
	text.resize(m_text.size());
	std::ranges::copy(m_text, begin(text));
	return text;
}

template<typename TextManipulator>
std::string BaseTextManipulator<TextManipulator>::getDisplayedText() const
{
	const auto& textManipulator{getImpl()};
	auto text{textManipulator.getDisplayedTextImpl()};

	if (m_textBoxProperties.getReplacementChar() != 0x00)
		std::ranges::fill(text, m_textBoxProperties.getReplacementChar());

	return text;
}

template<typename TextManipulator>
void BaseTextManipulator<TextManipulator>::addCh(int ch)
{
	if (m_text.size() == m_textBoxProperties.getMaxLength())
		return;

	auto& textManipulator{getImpl()};
	textManipulator.addChImpl(ch);
}

template<typename TextManipulator>
const TextManipulator& BaseTextManipulator<TextManipulator>::getImpl() const
{
	return static_cast<const TextManipulator&>(*this);
}

template<typename TextManipulator>
TextManipulator& BaseTextManipulator<TextManipulator>::getImpl()
{
	return static_cast<TextManipulator&>(*this);
}

namespace
{
std::vector<Text> transformTextToMatrix(const Text& text, unsigned int textBoxHeight, unsigned int textBoxWidth)
{
	std::vector<Text> textMatrix;

	textMatrix.resize(textBoxHeight, Text(textBoxWidth, 0x00));

	auto i{0u};
	for (auto line{0u}; line < textBoxHeight && i < text.size(); line++)
	{
		for (auto col{0u}; col < textBoxWidth && i < text.size(); col++)
		{
			textMatrix[line][col] = text[i];
			if (text[i++] == '\n')
				break;
		}
	}
	return textMatrix;
}

unsigned int findMatrixEndCol(const std::vector<Text>& textMatrix, unsigned int line)
{
	auto it{std::ranges::find_if(textMatrix[line], [](const auto& chAndOffset) {
		return chAndOffset == 0x00 || chAndOffset == '\n';
	})};
	if (it == textMatrix[line].end())
		return textMatrix[line].size() - 1;
	return it - textMatrix[line].begin();
}

unsigned int findMatrixOffset(const std::vector<Text>& textMatrix, Point cursorPos)
{
	auto y{cursorPos.getY()};
	auto x{cursorPos.getX()};

	auto offset{0u};
	for (auto line{0u}; line < y; line++)
	{
		auto endCol{findMatrixEndCol(textMatrix, line)};
		offset += textMatrix[line][endCol] == 0x00 ? endCol - 1 : endCol;
		offset++;
	}
	return offset + x;
}
}

template class TextManipulator<ScrollMechanism::WITHOUT_SCROLL>;

template<ScrollMechanism scrollMechanism>
TextManipulator<scrollMechanism>::TextManipulator(Size textBoxSize, TextBoxProperties&& textBoxProperties)
	 : BaseTextManipulator<TextManipulator<scrollMechanism>>{textBoxSize, std::move(textBoxProperties)}
{}

template<ScrollMechanism scrollMechanism>
void TextManipulator<scrollMechanism>::moveCursorPos(UpPos)
{
	auto y{this->m_cursorPos.getY()};
	auto x{this->m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(this->m_text, this->m_textBoxHeight, this->m_textBoxWidth)};
	if (y > 0)
	{
		auto prevLineEndCol{findMatrixEndCol(textMatrixForm, y - 1)};
		this->m_cursorPos = Point{Y{y - 1}, X{std::min(prevLineEndCol, x)}};
	}
}

template<ScrollMechanism scrollMechanism>
void TextManipulator<scrollMechanism>::moveCursorPos(RightPos)
{
	auto y{this->m_cursorPos.getY()};
	auto x{this->m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(this->m_text, this->m_textBoxHeight, this->m_textBoxWidth)};
	if (this->m_cursorPos != Point{Y{this->m_textBoxHeight - 1}, X{this->m_textBoxWidth - 1}})
	{
		if (textMatrixForm[y][x] == '\n' || x == this->m_textBoxWidth - 1)
			this->m_cursorPos = Point{Y{y + 1}, X{0u}};
		else if (textMatrixForm[y][x] != 0x00)
			this->m_cursorPos = Point{Y{y}, X{x + 1}};
	}
}

template<ScrollMechanism scrollMechanism>
void TextManipulator<scrollMechanism>::moveCursorPos(DownPos)
{
	auto y{this->m_cursorPos.getY()};
	auto x{this->m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(this->m_text, this->m_textBoxHeight, this->m_textBoxWidth)};
	auto currLineEndCol{findMatrixEndCol(textMatrixForm, y)};
	if (y + 1 < this->m_textBoxHeight &&
		 (currLineEndCol == this->m_textBoxWidth - 1 || textMatrixForm[y][currLineEndCol] == '\n'))
	{
		auto nextLineEndCol{findMatrixEndCol(textMatrixForm, y + 1)};
		this->m_cursorPos = Point{Y{y + 1}, X{std::min(nextLineEndCol, x)}};
	}
}

template<ScrollMechanism scrollMechanism>
void TextManipulator<scrollMechanism>::moveCursorPos(LeftPos)
{
	auto y{this->m_cursorPos.getY()};
	auto x{this->m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(this->m_text, this->m_textBoxHeight, this->m_textBoxWidth)};
	if (y > 0 && x == 0)
	{
		auto prevLineEndCol{findMatrixEndCol(textMatrixForm, y - 1)};
		this->m_cursorPos = Point{Y{y - 1}, X{prevLineEndCol}};
	}
	else if (x > 0)
		this->m_cursorPos = Point{Y{y}, X{x - 1}};
}

template<ScrollMechanism scrollMechanism>
void TextManipulator<scrollMechanism>::removeCh(BackwardsRemove)
{
	auto textMatrixForm{transformTextToMatrix(this->m_text, this->m_textBoxHeight, this->m_textBoxWidth)};
	auto y{this->m_cursorPos.getY()};
	auto x{this->m_cursorPos.getX()};
	if (y > 0 || x != 0)
	{
		unsigned int offset{0u};
		if (x == 0)
		{
			auto prevLineEndCol{findMatrixEndCol(textMatrixForm, y - 1)};
			offset = findMatrixOffset(textMatrixForm, Point{Y{y - 1}, X{prevLineEndCol}});
		}
		else
			offset = findMatrixOffset(textMatrixForm, Point{Y{y}, X{x - 1}});
		this->m_text.erase(this->m_text.begin() + offset);
	}
}

template<ScrollMechanism scrollMechanism>
void TextManipulator<scrollMechanism>::removeCh(ForwardsRemove)
{
	auto textMatrixForm{transformTextToMatrix(this->m_text, this->m_textBoxHeight, this->m_textBoxWidth)};
	auto offset{findMatrixOffset(textMatrixForm, this->m_cursorPos)};
	if (!this->m_text.empty() && this->m_text.begin() + offset != this->m_text.end())
		this->m_text.erase(this->m_text.begin() + offset);
}

template<ScrollMechanism scrollMechanism>
void TextManipulator<scrollMechanism>::addChImpl(int ch)
{
	auto textMatrixForm{transformTextToMatrix(this->m_text, this->m_textBoxHeight, this->m_textBoxWidth)};
	auto offset{findMatrixOffset(textMatrixForm, this->m_cursorPos)};
	auto y{this->m_cursorPos.getY()};
	if ((this->m_cursorPos != Point{Y{this->m_textBoxHeight - 1}, X{this->m_textBoxWidth - 1}} ||
		  textMatrixForm[this->m_textBoxHeight - 1][this->m_textBoxWidth - 1] == 0x00) &&
		 (y != this->m_textBoxHeight - 1 || ch != '\n'))
		this->m_text.insert(this->m_text.begin() + offset, ch);
}

template<ScrollMechanism scrollMechanism>
std::string TextManipulator<scrollMechanism>::getDisplayedTextImpl() const
{
	std::string text;

	text.resize(this->m_text.size());
	std::ranges::copy(this->m_text, begin(text));
	return text;
}

namespace
{
std::vector<Text> transformTextToMatrix(const Text& text, unsigned int width)
{
	std::vector<Text> textMatrix(1, Text(width, 0x00));

	auto line{0u};
	auto col{0u};

	for (auto i{0u}; i < text.size(); i++)
	{
		if (col == width - 1 || text[i] == '\n')
		{
			textMatrix[line][col] = text[i];
			textMatrix.push_back(Text(width, 0x00));
			line++;
			col = 0;
		}
		else
		{
			textMatrix[line][col] = text[i];
			col++;
		}
	}
	return textMatrix;
}
}

TextManipulator<ScrollMechanism::WITH_SCROLL>::TextManipulator(Size textBoxSize, TextBoxProperties&& textBoxProperties)
	 : BaseTextManipulator{textBoxSize, std::move(textBoxProperties)},
		m_slidingWindow{std::make_pair(0u, textBoxSize.getHeight())}
{}

void TextManipulator<ScrollMechanism::WITH_SCROLL>::moveCursorPos(UpPos)
{
	auto y{m_cursorPos.getY()};
	auto x{m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto& [minLine, maxLine]{m_slidingWindow};
	auto virtualLine{minLine + y};
	if (virtualLine > 0)
	{
		auto prevLineEndCol{findMatrixEndCol(textMatrixForm, virtualLine - 1)};
		auto minCol{std::min(prevLineEndCol, x)};
		if (y == 0)
		{
			m_cursorPos = Point{Y{y}, X{minCol}};
			minLine--;
			maxLine--;
		}
		else
			m_cursorPos = Point{Y{y - 1}, X{minCol}};
	}
}

void TextManipulator<ScrollMechanism::WITH_SCROLL>::moveCursorPos(RightPos)
{
	auto y{m_cursorPos.getY()};
	auto x{m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto& [minLine, maxLine]{m_slidingWindow};
	auto virtualLine{minLine + y};
	if (textMatrixForm[virtualLine][x] == '\n' || x == m_textBoxWidth - 1)
		if (y == m_textBoxHeight - 1)
		{
			m_cursorPos = Point{Y{y}, X{0u}};
			minLine++;
			maxLine++;
		}
		else
			m_cursorPos = Point{Y{y + 1}, X{0u}};
	else if (textMatrixForm[virtualLine][x] != 0x00)
		m_cursorPos = Point{Y{y}, X{x + 1}};
}

void TextManipulator<ScrollMechanism::WITH_SCROLL>::moveCursorPos(DownPos)
{
	auto y{m_cursorPos.getY()};
	auto x{m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto& [minLine, maxLine]{m_slidingWindow};
	auto virtualLine{minLine + y};
	auto currLineEndCol{findMatrixEndCol(textMatrixForm, virtualLine)};
	if ((currLineEndCol == m_textBoxWidth - 1 || textMatrixForm[virtualLine][currLineEndCol] == '\n'))
	{
		auto nextLineEndCol{findMatrixEndCol(textMatrixForm, virtualLine + 1)};
		auto minCol{std::min(nextLineEndCol, x)};
		if (y == m_textBoxHeight - 1)
		{
			m_cursorPos = Point{Y{y}, X{minCol}};
			minLine++;
			maxLine++;
		}
		else
			m_cursorPos = Point{Y{y + 1}, X{minCol}};
	}
}

void TextManipulator<ScrollMechanism::WITH_SCROLL>::moveCursorPos(LeftPos)
{
	auto y{m_cursorPos.getY()};
	auto x{m_cursorPos.getX()};
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto& [minLine, maxLine]{m_slidingWindow};
	auto virtualLine{minLine + y};
	if (virtualLine > 0 && x == 0)
	{
		auto prevLineEndCol{findMatrixEndCol(textMatrixForm, virtualLine - 1)};
		if (y == 0)
		{
			m_cursorPos = Point{Y{y}, X{prevLineEndCol}};
			minLine--;
			maxLine--;
		}
		else
			m_cursorPos = Point{Y{y - 1}, X{prevLineEndCol}};
	}
	else if (x > 0)
		m_cursorPos = Point{Y{y}, X{x - 1}};
}

void TextManipulator<ScrollMechanism::WITH_SCROLL>::removeCh(BackwardsRemove)
{
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto y{m_cursorPos.getY()};
	auto x{m_cursorPos.getX()};
	auto [minLine, maxLine]{m_slidingWindow};
	auto virtualLine{minLine + y};
	if (virtualLine > 0 || x != 0)
	{
		unsigned int offset{0u};
		if (x == 0)
		{
			auto prevLineEndCol{findMatrixEndCol(textMatrixForm, virtualLine - 1)};
			offset = findMatrixOffset(textMatrixForm, Point{Y{virtualLine - 1}, X{prevLineEndCol}});
		}
		else
			offset = findMatrixOffset(textMatrixForm, Point{Y{virtualLine}, X{x - 1}});
		m_text.erase(m_text.begin() + offset);
	}
}

void TextManipulator<ScrollMechanism::WITH_SCROLL>::removeCh(ForwardsRemove)
{
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto y{m_cursorPos.getY()};
	auto x{m_cursorPos.getX()};
	auto [minLine, maxLine]{m_slidingWindow};
	auto virtualLine{minLine + y};
	auto offset{findMatrixOffset(textMatrixForm, Point{Y{virtualLine}, X{x}})};
	if (!m_text.empty() && m_text.begin() + offset != m_text.end())
		m_text.erase(m_text.begin() + offset);
}

void TextManipulator<ScrollMechanism::WITH_SCROLL>::addChImpl(int ch)
{
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto y{m_cursorPos.getY()};
	auto x{m_cursorPos.getX()};
	auto [minLine, maxLine]{m_slidingWindow};
	auto virtualLine{minLine + y};
	auto offset{findMatrixOffset(textMatrixForm, Point{Y{virtualLine}, X{x}})};
	m_text.insert(m_text.begin() + offset, ch);
}

std::string TextManipulator<ScrollMechanism::WITH_SCROLL>::getDisplayedTextImpl() const
{
	auto textMatrixForm{transformTextToMatrix(m_text, m_textBoxWidth)};
	auto [minLine, maxLine]{m_slidingWindow};
	std::string text;
	for (auto i{minLine}; i < maxLine && i < textMatrixForm.size(); i++)
		std::ranges::copy_if(textMatrixForm[i], std::back_inserter(text), [](auto ch) {
			return ch != 0x00;
		});

	return text;
}
}
