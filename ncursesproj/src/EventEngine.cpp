#include "EventEngine.hpp"

#include "KeyEventObserver.hpp"
#include "Widget.hpp"

namespace ncursesplus::event
{
void EventEngine::launch(widget::WidgetCommon& root)
{
	noecho();
	nonl();
	while (int ch = widget::readInput(&root))
		root.handleKeyEvent(KeyEvent{ch});
}
}
