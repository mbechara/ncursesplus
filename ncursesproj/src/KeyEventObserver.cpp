#include "KeyEventObserver.hpp"

namespace ncursesplus::event
{
KeyEvent::KeyEvent(int key)
	 : m_key{key}
{}

int KeyEvent::getKey() const
{
	return m_key;
}

bool KeyEvent::isKeyCombination() const
{
	auto ctrlCharacter = m_key + 0x40;
	return ctrlCharacter >= 0x40 && ctrlCharacter <= 0x5f && ctrlCharacter != 0x4d;
}
}
