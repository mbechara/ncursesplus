#include "Widget.hpp"
#include "Window.hpp"

#include "Mediator.hpp"

#include <stdexcept>

namespace ncursesplus::widget
{
std::string Widget::m_activeWidget = "";

using namespace literals;

Widget::Widget(const std::string& name, Size size, Point point, customization::WidgetColor&& widgetColor)
	 : WidgetCommon{name, size, point, std::move(widgetColor)}
{}

bool Widget::isActiveWidget() const
{
	return m_activeWidget == getName();
}

bool Widget::handleKeyEvent(event::KeyEvent event)
{
	if (!isActiveWidget())
		return false;

	if (!event.isKeyCombination())
		handleKeyEventImpl(event);

	if (!m_mediator)
		return false;

	switch (event.getKey())
	{
		case 0x0b:   // Ctrl+k
			m_mediator->notify(this, "TOP");
			break;
		case 0x0c:   // Ctrl+l
			m_mediator->notify(this, "RIGHT");
			break;
		case 0x0a:   // Ctrl+j
			m_mediator->notify(this, "BOTTOM");
			break;
		case 0x08:   // Ctrl+h
			m_mediator->notify(this, "LEFT");
			break;
	}

	return true;
}

void Widget::addMediator(std::shared_ptr<Mediator> mediator)
{
	if (mediator)
	{
		m_mediator = mediator;
		m_mediator->add(this);
	}
}

void Widget::moveCursor() const
{
	m_activeWidget = getName();
	curs_set(1);
	moveCursorImpl();
}

int Widget::read() const
{
	if (!isActiveWidget())
		throw std::logic_error{"No active widget"};
	return readImpl();
}

int Widget::readImpl() const
{
	return wgetch(m_window->getNcursesWindow());
}

const WidgetCommon* find(const WidgetCommon* widget, const std::string& name)
{
	return widget->find([name](const auto* widget) {
		return widget->getName() == name;
	});
}

const Widget* findActiveWidget(const WidgetCommon* widget)
{
	const auto* activeWidget{widget->find([](const auto* widget) {
		return widget->isActiveWidget();
	})};
	return static_cast<const Widget*>(activeWidget);
}

int readInput(const WidgetCommon* widget)
{
	const auto* activeWidget{findActiveWidget(widget)};
	return activeWidget->read();
}
}
