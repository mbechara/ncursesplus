#include "WidgetCommon.hpp"
#include "Point.hpp"
#include "Size.hpp"
#include "Window.hpp"

#include <ncurses.h>

namespace ncursesplus::widget
{
WidgetCommon::~WidgetCommon() = default;

WidgetCommon::WidgetCommon(const std::string& name, Size size, Point point, customization::WidgetColor&& widgetColor)
	 : m_name{name},
		m_window{std::make_unique<window::Window>(size, point)}
{
	widgetColor.apply(m_window.get());
}

std::string_view WidgetCommon::getName() const
{
	return m_name;
}

Size WidgetCommon::getSize() const
{
	return m_window->getSize();
}

Point WidgetCommon::getPoint() const
{
	return m_window->getPoint();
}

namespace
{
/*
void drawName(WINDOW* innerWindow, const std::string& name, WidgetNameAlignment widgetNameAlignment)
{
	auto innerWindowWidth = innerWindow->_maxx;
	unsigned int nameOffset{0u};
	switch (widgetNameAlignment)
	{
		case WidgetNameAlignment::LEFT:
			nameOffset = 1;
			break;
		case WidgetNameAlignment::CENTER:
			nameOffset = innerWindowWidth / 2 - name.size() / 2;
			break;
		case WidgetNameAlignment::RIGHT:
			nameOffset = innerWindowWidth - name.size();
			break;
	}
	mvwaddstr(innerWindow, 0, nameOffset, name.c_str());
}
*/
}

void WidgetCommon::draw() const
{
	auto ncursesWindow{m_window->getNcursesWindow()};
	box(ncursesWindow, 0, 0);
	// drawName(ncursesWindow, m_name, m_widgetNameAlignment);
	wrefresh(ncursesWindow);
	drawImpl();
}

const WidgetCommon* WidgetCommon::find(std::function<bool(const WidgetCommon*)> predicate) const
{
	return predicate(this) ? this : nullptr;
}

void WidgetCommon::traverse(const std::function<void(const WidgetCommon*)>& callback) const
{
	callback(this);
}
}
