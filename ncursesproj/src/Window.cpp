#include "Window.hpp"
#include <ncurses.h>

namespace ncursesplus::window
{
Window::Window(widget::Size size, widget::Point point)
	 : m_size{size},
		m_point{point}
{
	auto height{m_size.getHeight()};
	auto width{m_size.getWidth()};
	m_ncursesWindow = newwin(height, width, m_point.getY(), m_point.getX());
}

widget::Size Window::getSize() const
{
	return m_size;
}

widget::Point Window::getPoint() const
{
	return m_point;
}

WINDOW* Window::getNcursesWindow() const
{
	return m_ncursesWindow;
}
}
