#include "TextBoxProperties.hpp"

namespace ncursesplus::widget
{
void TextBoxProperties::setReplacementChar(char replacementCh)
{
	m_replacementCh = replacementCh;
}

char TextBoxProperties::getReplacementChar() const
{
	return m_replacementCh;
}

void TextBoxProperties::setMaxLength(unsigned int maxLength)
{
	m_maxLength = maxLength;
}

unsigned int TextBoxProperties::getMaxLength() const
{
	return m_maxLength;
}
}
