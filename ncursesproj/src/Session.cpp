#include "Session.hpp"
#include <ncurses.h>
#include <stdexcept>

namespace ncursesplus
{
Session::Session()
{
	initscr();
	refresh();
}

Session::~Session()
{
	endwin();
}

Session& Session::withColor()
{
	if (!has_colors())
		throw std::logic_error{"Terminal does not support color"};
	start_color();
	use_default_colors();
	return *this;
}
}
