#include "TextBoxWidget.hpp"
#include "Window.hpp"

namespace ncursesplus::widget
{
template class TextBoxWidget<ScrollMechanism::WITHOUT_SCROLL>;
template class TextBoxWidget<ScrollMechanism::WITH_SCROLL>;

using namespace widget::literals;

template<ScrollMechanism scrollMechanism>
TextBoxWidget<scrollMechanism>::TextBoxWidget(const std::string& name, Size size, Point point,
															 TextBoxProperties&& textBoxProperties,
															 customization::WidgetColor&& widgetColor)
	 : Widget{name, size, point, std::move(widgetColor)},
		m_textManipulator{size - Size{2_h, 2_w}, std::move(textBoxProperties)},
		m_innerWindow{std::make_unique<window::Window>(size - Size{2_h, 2_w}, point + Point{1_y, 1_x})}
{
	keypad(m_innerWindow->getNcursesWindow(), true);
}

template<ScrollMechanism scrollMechanism>
TextBoxWidget<scrollMechanism>::~TextBoxWidget() = default;

template<ScrollMechanism scrollMechanism>
std::string TextBoxWidget<scrollMechanism>::getText() const
{
	return m_textManipulator.getFullText();
}

template<ScrollMechanism scrollMechanism>
Point TextBoxWidget<scrollMechanism>::getCursor() const
{
	unsigned int y{0u};
	unsigned int x{0u};
	getyx(m_innerWindow->getNcursesWindow(), y, x);
	return Point{Y{y}, X{x}};
}

template<ScrollMechanism scrollMechanism>
void TextBoxWidget<scrollMechanism>::handleKeyEventImpl(event::KeyEvent keyEvent)
{
	auto key{keyEvent.getKey()};
	auto cursorPos{getCursor()};

	m_textManipulator.setCursorPos(cursorPos);

	switch (key)
	{
		case KEY_UP:
			m_textManipulator.moveCursorPos(UpPos{});
			break;
		case KEY_RIGHT:
			m_textManipulator.moveCursorPos(RightPos{});
			break;
		case KEY_DOWN:
			m_textManipulator.moveCursorPos(DownPos{});
			break;
		case KEY_LEFT:
			m_textManipulator.moveCursorPos(LeftPos{});
			break;
		case KEY_BACKSPACE:
			m_textManipulator.removeCh(BackwardsRemove{});
			m_textManipulator.moveCursorPos(LeftPos{});
			break;
		case KEY_DC:
			m_textManipulator.removeCh(ForwardsRemove{});
			break;
		case KEY_ENTER:
		case '\r':
			m_textManipulator.addCh('\n');
			m_textManipulator.moveCursorPos(RightPos{});
			break;
		default:
			m_textManipulator.addCh(key);
			m_textManipulator.moveCursorPos(RightPos{});
			break;
	}

	const auto& displayedText = m_textManipulator.getDisplayedText();
	auto nextCursorPos{m_textManipulator.getCursorPos()};

	clearText();
	addText(displayedText);
	wmove(m_innerWindow->getNcursesWindow(), nextCursorPos.getY(), nextCursorPos.getX());
}

template<ScrollMechanism scrollMechanism>
void TextBoxWidget<scrollMechanism>::moveCursorImpl() const
{
	auto cursorPos = getCursor();
	auto* innerNcursesWindow{m_innerWindow->getNcursesWindow()};
	wmove(innerNcursesWindow, cursorPos.getY(), cursorPos.getX());
	wrefresh(innerNcursesWindow);
}

template<ScrollMechanism scrollMechanism>
int TextBoxWidget<scrollMechanism>::readImpl() const
{
	return wgetch(m_innerWindow->getNcursesWindow());
}

template<ScrollMechanism scrollMechanism>
void TextBoxWidget<scrollMechanism>::drawImpl() const
{
	auto innerNcursesWindow{m_innerWindow->getNcursesWindow()};
	wrefresh(innerNcursesWindow);
}

template<ScrollMechanism scrollMechanism>
void TextBoxWidget<scrollMechanism>::addText(const std::string& text) const
{
	waddstr(m_innerWindow->getNcursesWindow(), text.c_str());
}

template<ScrollMechanism scrollMechanism>
void TextBoxWidget<scrollMechanism>::clearText() const
{
	wclear(m_innerWindow->getNcursesWindow());
}
}
