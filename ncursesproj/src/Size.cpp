#include "Size.hpp"

namespace ncursesplus::widget
{
unsigned int Size::getHeight() const
{
	return m_height;
}

unsigned int Size::getWidth() const
{
	return m_width;
}

Size& Size::operator+=(const Size& rSize)
{
	m_height += rSize.getHeight();
	m_width += rSize.getWidth();
	return *this;
	;
}

Size& Size::operator-=(const Size& rSize)
{
	m_height -= rSize.getHeight();
	m_width -= rSize.getWidth();
	return *this;
	;
}

Size& Size::operator*=(const Size& rSize)
{
	m_height *= rSize.getHeight();
	m_width *= rSize.getWidth();
	return *this;
	;
}

Size& Size::operator/=(const Size& rSize)
{
	m_height /= rSize.getHeight();
	m_width /= rSize.getWidth();
	return *this;
	;
}

Size operator+(const Size& lSize, const Size& rSize)
{
	return Size{lSize} += rSize;
}

Size operator-(const Size& lSize, const Size& rSize)
{
	return Size{lSize} -= rSize;
}

Size operator*(const Size& lSize, const Size& rSize)
{
	return Size{lSize} *= rSize;
}

Size operator/(const Size& lSize, const Size& rSize)
{
	return Size{lSize} /= rSize;
}

}
