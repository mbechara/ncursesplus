#include "Mediator.hpp"

#include "Widget.hpp"

namespace ncursesplus::widget
{
void Mediator::add(Widget* widget)
{
	m_widgets.push_back(widget);
}
}
