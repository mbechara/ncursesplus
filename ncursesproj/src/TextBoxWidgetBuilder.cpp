#include "TextBoxWidgetBuilder.hpp"
#include "TextBoxWidget.hpp"

namespace ncursesplus::widget::builder
{
TextBoxWidgetBuilder::TextBoxWidgetBuilder(const std::string& name)
	 : WidgetBuilder{name}
{}

TextBoxWidgetBuilder& TextBoxWidgetBuilder::withScroll()
{
	m_scroll = true;
	return *this;
}

TextBoxWidgetBuilder& TextBoxWidgetBuilder::withCharReplacement(char replacementCh)
{
	m_textBoxProperties.setReplacementChar(replacementCh);
	return *this;
}

TextBoxWidgetBuilder& TextBoxWidgetBuilder::withMaxLength(unsigned int maxLength)
{
	m_textBoxProperties.setMaxLength(maxLength);
	return *this;
}

std::unique_ptr<Widget> TextBoxWidgetBuilder::buildImpl()
{
	std::unique_ptr<Widget> textBoxWidget;
	if (m_scroll)
		textBoxWidget = std::make_unique<TextBoxWidget<ScrollMechanism::WITH_SCROLL>>(
			m_name, m_size, m_point, std::move(m_textBoxProperties), customization::WidgetColor{});
	else
		textBoxWidget = std::make_unique<TextBoxWidget<ScrollMechanism::WITHOUT_SCROLL>>(
			m_name, m_size, m_point, std::move(m_textBoxProperties), customization::WidgetColor{});
	return textBoxWidget;
}
}
