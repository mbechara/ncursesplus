#include "NeighborWidgetsMediator.hpp"
#include "Point.hpp"
#include "Size.hpp"

#include "Helpers.hpp"
#include "Widget.hpp"

#include <cmath>
#include <functional>
#include <limits>
#include <ranges>

namespace ncursesplus::widget
{
namespace
{
struct WidgetCoordinates
{
	unsigned int m_rowStart;
	unsigned int m_rowEnd;
	unsigned int m_colStart;
	unsigned int m_colEnd;
	bool operator==(const WidgetCoordinates&) const = default;
};

WidgetCoordinates getWidgetCoordinates(const Widget* widget)
{
	auto widgetPoint{widget->getPoint()};

	auto widgetStartY{widgetPoint.getY()};
	auto widgetStartX{widgetPoint.getX()};

	auto widgetSize = widget->getSize();
	auto widgetEndY = widgetStartY + widgetSize.getHeight();
	auto widgetEndX = widgetStartX + widgetSize.getWidth();

	return WidgetCoordinates{widgetStartY, widgetEndY, widgetStartX, widgetEndX};
}

std::pair<float, float> getCenterCoordinates(const WidgetCoordinates& widgetCoordinates)
{
	auto [widgetRowStart, widgetRowEnd, widgetColStart, widgetColEnd] = widgetCoordinates;
	return {(widgetRowStart + widgetRowEnd) / 2.f, (widgetColStart + widgetColEnd) / 2.f};
}

float getDistance(std::pair<float, float> centerA, std::pair<float, float> centerB)
{
	auto [centerAY, centerAX] = centerA;
	auto [centerBY, centerBX] = centerB;
	return std::sqrt(std::pow(centerBY - centerAY, 2) + std::pow(centerBX - centerAX, 2));
}

WidgetCoordinates stretchWidgetAToWidgetB(const WidgetCoordinates& widgetA, const WidgetCoordinates& widgetB,
														const std::string& side)
{
	if (side == "TOP")
		return WidgetCoordinates{widgetA.m_rowStart, widgetB.m_rowEnd, widgetA.m_colStart, widgetA.m_colEnd};
	else if (side == "RIGHT")
		return WidgetCoordinates{widgetA.m_rowStart, widgetA.m_rowEnd, widgetB.m_colStart, widgetA.m_colEnd};
	else if (side == "BOTTOM")
		return WidgetCoordinates{widgetB.m_rowStart, widgetA.m_rowEnd, widgetA.m_colStart, widgetA.m_colEnd};
	else
		return WidgetCoordinates{widgetA.m_rowStart, widgetA.m_rowEnd, widgetA.m_colStart, widgetB.m_colEnd};
}
}

void NeighborWidgetsMediator::notify(const Widget* sender, const std::string& side)
{
	Widget* closestNeighbor = nullptr;
	auto isSender = [sender](auto* widget) {
		return widget == sender;
	};

	auto senderWidgetCoordinates = getWidgetCoordinates(sender);
	auto senderWidgetCenterCoordinates = getCenterCoordinates(senderWidgetCoordinates);
	auto filteredWidgets = m_widgets | std::views::filter(std::not_fn(isSender));

	for (auto minDistance = std::numeric_limits<float>::max(); auto* neighborWidget: filteredWidgets)
	{
		auto neighborWidgetCoordinates = getWidgetCoordinates(neighborWidget);
		auto neighborWidgetCenterCoordinates = getCenterCoordinates(neighborWidgetCoordinates);
		auto distance = getDistance(neighborWidgetCenterCoordinates, senderWidgetCenterCoordinates);

		auto stretchedNeighborWidgetCoordinates =
			stretchWidgetAToWidgetB(neighborWidgetCoordinates, senderWidgetCoordinates, side);
		if (intersectionArea(stretchedNeighborWidgetCoordinates, senderWidgetCoordinates) > 0 && distance < minDistance)
		{
			minDistance = distance;
			closestNeighbor = neighborWidget;
		}
	}

	if (closestNeighbor)
		closestNeighbor->moveCursor();
}
}
