#include "Point.hpp"

namespace ncursesplus::widget
{
unsigned int Point::getY() const
{
	return m_y;
}

unsigned int Point::getX() const
{
	return m_x;
}

Point& Point::operator+=(const Point& rPoint)
{
	m_y += rPoint.m_y;
	m_x += rPoint.m_x;
	return *this;
	;
}

Point& Point::operator-=(const Point& rPoint)
{
	m_y -= rPoint.m_y;
	m_x -= rPoint.m_x;
	return *this;
	;
}

Point& Point::operator*=(const Point& rPoint)
{
	m_y *= rPoint.m_y;
	m_x *= rPoint.m_x;
	return *this;
	;
}

Point& Point::operator/=(const Point& rPoint)
{
	m_y /= rPoint.m_y;
	m_x /= rPoint.m_x;
	return *this;
	;
}

Point operator+(const Point& lPoint, const Point& rPoint)
{
	return Point{lPoint} += rPoint;
}

Point operator-(const Point& lPoint, const Point& rPoint)
{
	return Point{lPoint} -= rPoint;
}

Point operator*(const Point& lPoint, const Point& rPoint)
{
	return Point{lPoint} *= rPoint;
}

Point operator/(const Point& lPoint, const Point& rPoint)
{
	return Point{lPoint} /= rPoint;
}
}
