#include "WidgetColor.hpp"
#include "Window.hpp"
#include <ncurses.h>
#include <unordered_map>

namespace ncursesplus::widget::customization
{
const std::unordered_map<Color, short> ColorMapping{{Color::DEFAULT, -1},
																	 {Color::BLACK, COLOR_BLACK},
																	 {Color::RED, COLOR_RED},
																	 {Color::GREEN, COLOR_GREEN},
																	 {Color::YELLOW, COLOR_YELLOW},
																	 {Color::BLUE, COLOR_BLUE},
																	 {Color::MAGENTA, COLOR_MAGENTA},
																	 {Color::CYAN, COLOR_CYAN},
																	 {Color::WHITE, COLOR_WHITE}};

void WidgetColor::apply(const window::Window* window) const
{
	init_pair(m_pairIndex, ColorMapping.at(m_foregroundColor), ColorMapping.at(m_backgroundColor));
	wbkgd(window->getNcursesWindow(), COLOR_PAIR(m_pairIndex));
}
}
