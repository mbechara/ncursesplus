#include "CustomGridLayoutStrategy.hpp"
#include "LayoutStrategy.hpp"

namespace ncursesplus::layout
{
using namespace widget;

widget::Size CustomGridLayoutStrategy::computeSize(LayoutConstraintType customGridLayoutConstraint) const
{
	auto rowSpan = customGridLayoutConstraint.m_rowEnd - customGridLayoutConstraint.m_rowStart;
	auto colSpan = customGridLayoutConstraint.m_colEnd - customGridLayoutConstraint.m_colStart;

	auto numOfRows{getNumOfRows()};
	auto numOfCols{getNumOfCols()};

	auto parentSize{getParentSize()};

	auto rowSize{parentSize.getHeight() / numOfRows};
	auto colSize{parentSize.getWidth() / numOfCols};

	auto height{rowSize * rowSpan};
	auto width{colSize * colSpan};

	return Size{Height{height}, Width{width}};
}

widget::Point CustomGridLayoutStrategy::computePoint(LayoutConstraintType customGridLayoutConstraint) const
{
	auto numOfRows{getNumOfRows()};
	auto numOfCols{getNumOfCols()};

	auto parentSize{getParentSize()};

	auto rowSize{parentSize.getHeight() / numOfRows};
	auto colSize{parentSize.getWidth() / numOfCols};

	auto parentPoint{getParentPoint()};

	auto beginY = rowSize * customGridLayoutConstraint.m_rowStart + parentPoint.getY();
	auto beginX = colSize * customGridLayoutConstraint.m_colStart + parentPoint.getX();

	return Point{Y{beginY}, X{beginX}};
}
}
