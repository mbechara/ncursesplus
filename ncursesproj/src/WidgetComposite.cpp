#include "WidgetComposite.hpp"
#include "Point.hpp"
#include "Size.hpp"
#include "Widget.hpp"

namespace ncursesplus::widget
{
WidgetComposite::WidgetComposite(const std::string& name, Size size, Point point,
											customization::WidgetColor&& widgetColor)
	 : WidgetCommon{name, size, point, std::move(widgetColor)}
{}

void WidgetComposite::add(std::unique_ptr<WidgetCommon> widget)
{
	m_widgets.push_back(std::move(widget));
}

bool WidgetComposite::handleKeyEvent(event::KeyEvent keyEvent)
{
	using namespace widget::literals;

	for (auto& widgets: m_widgets)
		if (widgets->handleKeyEvent(keyEvent))
			return true;
	return false;
}

const WidgetCommon* WidgetComposite::find(std::function<bool(const WidgetCommon*)> predicate) const
{
	if (WidgetCommon::find(predicate))
		return this;

	for (const auto& widgets: m_widgets)
		if (const auto* foundWidget = widgets->find(predicate))
			return foundWidget;

	return nullptr;
}

void WidgetComposite::traverse(const std::function<void(const WidgetCommon*)>& callback) const
{
	WidgetCommon::traverse(callback);
	for (const auto& widgets: m_widgets)
		widgets->traverse(callback);
}

bool WidgetComposite::isActiveWidget() const
{
	return false;
}

void WidgetComposite::drawImpl() const
{
	for (const auto& widgets: m_widgets)
		widgets->draw();
	const auto* activeWidget{findActiveWidget(this)};
	activeWidget->draw();
}
}
