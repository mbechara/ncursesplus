#ifndef WIDGETBUILDERCOMMON_HPP
#define WIDGETBUILDERCOMMON_HPP
#include "LayoutStrategy.fwd.hpp"
#include "Point.hpp"
#include "Size.hpp"
#include "WidgetCommon.fwd.hpp"

#include <memory>
#include <string>

namespace ncursesplus::widget::builder
{
class WidgetBuilderCommon
{
public:
	WidgetBuilderCommon(const std::string& name);
	virtual std::unique_ptr<WidgetCommon> build() = 0;
private:
	template<unsigned int, layout::IsLayoutStrategy>
	friend class WidgetCompositeBuilder;
	virtual unsigned int getNumOfActiveWidgets() const = 0;
protected:
	std::string m_name;
	Size m_size;
	Point m_point;
};
}
#endif
