#ifndef HELPERS_HPP
#define HELPERS_HPP
#include <algorithm>
#include <string>

template<typename T>
float intersectionArea(T boxACoordinates, T boxBCoordinates)
{
	auto intersectY = std::max<int>(0, std::min(boxACoordinates.m_rowEnd, boxBCoordinates.m_rowEnd) -
													  std::max(boxACoordinates.m_rowStart, boxBCoordinates.m_rowStart));
	auto intersectX = std::max<int>(0, std::min(boxACoordinates.m_colEnd, boxBCoordinates.m_colEnd) -
													  std::max(boxACoordinates.m_colStart, boxBCoordinates.m_colStart));
	auto intersectionArea = intersectY * intersectX;

	return intersectionArea;
}

template<typename... T>
concept AreStrings = (std::is_same_v<T, std::string> && ...);

template<typename T>
consteval T pow(T base, T exponent)
{
	if (exponent == 0)
		return 1;
	for (auto i{1}; i <= exponent - 1; i++)
		base *= base;
	return base;
}
#endif
