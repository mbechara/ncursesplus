#ifndef GRIDLAYOUTSTRATEGYHELPERS_HPP
#define GRIDLAYOUTSTRATEGYHELPERS_HPP
#include "GridLayoutStrategy.fwd.hpp"
#include <vector>

namespace ncursesplus::layout
{
struct GridPosition
{
	unsigned int rowStart{};
	unsigned int rowEnd{};
	unsigned int colStart{};
	unsigned int colEnd{};
};

using CellAvailabilityMatrix = std::vector<std::vector<bool>>;

CellAvailabilityMatrix registerCells(CellAvailabilityMatrix&&, GridPosition);

GridPosition computeVerticalGridPosition(const CellAvailabilityMatrix&);
GridPosition computeHorizontalGridPosition(const CellAvailabilityMatrix&);
GridPosition computeSingleGridPosition(const CellAvailabilityMatrix&);

template<IsGridLayoutConstraint GridLayoutConstraint>
GridPosition computeGridPosition(const CellAvailabilityMatrix& cellAvailabilityMatrix,
											GridLayoutConstraint gridLayoutConstraint)
{
	switch (gridLayoutConstraint)
	{
		case GridLayoutConstraint::VERTICAL:
			return computeVerticalGridPosition(cellAvailabilityMatrix);
			break;
		case GridLayoutConstraint::HORIZONTAL:
			return computeHorizontalGridPosition(cellAvailabilityMatrix);
			break;
		case GridLayoutConstraint::NO_CONSTRAINT:
			return computeSingleGridPosition(cellAvailabilityMatrix);
			break;
	}
	return {};
}
}
#endif
