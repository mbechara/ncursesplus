#ifndef SIDEBARWIDGETBUILDER_HPP
#define SIDEBARWIDGETBUILDER_HPP
#include "SideBarWidget.hpp"
#include "WidgetBuilder.hpp"

namespace ncursesplus::widget::builder
{
template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
class SideBarWidgetBuilder final : public WidgetBuilder<SideBarWidgetBuilder<NumMenuEntries, MenuEntryPercentage>>
{
	static_assert(MenuEntryPercentage != 0u && MenuEntryPercentage % 10 == 0 &&
					  NumMenuEntries * MenuEntryPercentage <= 100u && NumMenuEntries >= 1u);
public:
	SideBarWidgetBuilder(const std::string&);
	template<AreStrings... T>
	SideBarWidgetBuilder& withMenuEntries(const T&...);
private:
	std::unique_ptr<Widget> buildImpl() override;
private:
	std::array<std::string, NumMenuEntries> m_menuEntries;
	unsigned int m_insertIndex{0u};
};

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
SideBarWidgetBuilder<NumMenuEntries, MenuEntryPercentage>::SideBarWidgetBuilder(const std::string& name)
	 : WidgetBuilder<SideBarWidgetBuilder<NumMenuEntries, MenuEntryPercentage>>{name}
{}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
template<AreStrings... T>
SideBarWidgetBuilder<NumMenuEntries, MenuEntryPercentage>&
SideBarWidgetBuilder<NumMenuEntries, MenuEntryPercentage>::withMenuEntries(const T&... menuEntries)
{
	static_assert(sizeof...(menuEntries) == NumMenuEntries);
	unsigned int i{0u};
	for (const auto& menuEntry: {menuEntries...})
	{
		m_menuEntries[i] = menuEntry;
		i++;
	}
	return *this;
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
std::unique_ptr<Widget> SideBarWidgetBuilder<NumMenuEntries, MenuEntryPercentage>::buildImpl()
{
	auto sideBarWidget{std::make_unique<SideBarWidget<NumMenuEntries, MenuEntryPercentage>>(
		this->m_name, this->m_size, this->m_point, customization::WidgetColor{})};

	using Indices = std::make_index_sequence<NumMenuEntries>;
	[this, &sideBarWidget]<size_t... I>(std::index_sequence<I...>) {
		return sideBarWidget->addMenuEntries(m_menuEntries[I]...);
	}(Indices{});

	return sideBarWidget;
}
}
#endif
