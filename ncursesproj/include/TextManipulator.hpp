#ifndef TEXTMANIPULATOR_HPP
#define TEXTMANIPULATOR_HPP
#include "Point.hpp"
#include "Size.fwd.hpp"
#include "TextBoxProperties.hpp"

#include <string>
#include <vector>

namespace ncursesplus::widget
{
enum class ScrollMechanism
{
	WITH_SCROLL,
	WITHOUT_SCROLL
};

using Text = std::vector<int>;

struct UpPos
{};
struct RightPos
{};
struct DownPos
{};
struct LeftPos
{};

struct BackwardsRemove
{};
struct ForwardsRemove
{};

template<typename TextManipulator>
class BaseTextManipulator
{
public:
	BaseTextManipulator(Size, TextBoxProperties&& = {});
	void setCursorPos(Point);
	Point getCursorPos() const;
	std::string getFullText() const;
	std::string getDisplayedText() const;
	void addCh(int);
private:
	const TextManipulator& getImpl() const;
	TextManipulator& getImpl();
protected:
	Text m_text;
	Point m_cursorPos;
	unsigned int m_textBoxHeight;
	unsigned int m_textBoxWidth;
	TextBoxProperties m_textBoxProperties;
};

template<ScrollMechanism scrollMechanism = ScrollMechanism::WITHOUT_SCROLL>
class TextManipulator : public BaseTextManipulator<TextManipulator<scrollMechanism>>
{
public:
	TextManipulator(Size, TextBoxProperties&& = {});
	void moveCursorPos(UpPos);
	void moveCursorPos(RightPos);
	void moveCursorPos(DownPos);
	void moveCursorPos(LeftPos);
	void removeCh(BackwardsRemove);
	void removeCh(ForwardsRemove);
private:
	friend class BaseTextManipulator<TextManipulator<scrollMechanism>>;
	std::string getDisplayedTextImpl() const;
	void addChImpl(int);
};

template<>
class TextManipulator<ScrollMechanism::WITH_SCROLL>
	 : public BaseTextManipulator<TextManipulator<ScrollMechanism::WITH_SCROLL>>
{
public:
	TextManipulator(Size, TextBoxProperties&& = {});
	void moveCursorPos(UpPos);
	void moveCursorPos(RightPos);
	void moveCursorPos(DownPos);
	void moveCursorPos(LeftPos);
	void removeCh(BackwardsRemove);
	void removeCh(ForwardsRemove);
private:
	friend class BaseTextManipulator<TextManipulator<ScrollMechanism::WITH_SCROLL>>;
	std::string getDisplayedTextImpl() const;
	void addChImpl(int);
private:
	std::pair<unsigned int, unsigned int> m_slidingWindow;
};
}
#endif
