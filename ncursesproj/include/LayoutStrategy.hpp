#ifndef LAYOUTSTRATEGY_HPP
#define LAYOUTSTRATEGY_HPP
#include "LayoutStrategy.fwd.hpp"
#include "Point.hpp"
#include "Size.hpp"

#include <array>
#include <cassert>

namespace ncursesplus::layout
{
template<unsigned int NumOfRows>
using Rows = PhantomValue<unsigned int, struct RowsParameter, NumOfRows>;

template<unsigned int NumOfCols>
using Cols = PhantomValue<unsigned int, struct ColsParameter, NumOfCols>;

template<typename LayoutStrategyImpl>
class LayoutStrategy
{
public:
	using LayoutConstraintType = typename LayoutStrategyTraits<LayoutStrategyImpl>::LayoutConstraintType;
	void setParentSize(widget::Size);
	void setParentPoint(widget::Point);
	template<layout::ProperLayoutConstraint<LayoutStrategyImpl> LayoutConstraint, unsigned int NumOfConstraints>
	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
	layoutWidgets(const LayoutConstraint (&)[NumOfConstraints]) const;
protected:
	template<unsigned int NumOfRows, unsigned int NumOfCols>
	consteval LayoutStrategy(Rows<NumOfRows>, Cols<NumOfCols>);
	widget::Size getParentSize() const;
	widget::Point getParentPoint() const;
	unsigned int getNumOfRows() const;
	unsigned int getNumOfCols() const;
private:
	const LayoutStrategyImpl& getImpl() const;
	LayoutStrategyImpl& getImpl();
private:
	widget::Size m_parentSize;
	widget::Point m_parentPoint;
	unsigned int m_numOfRows;
	unsigned int m_numOfCols;
};

template<typename LayoutStrategyImpl>
void LayoutStrategy<LayoutStrategyImpl>::setParentSize(widget::Size parentSize)
{
	m_parentSize = parentSize;
}

template<typename LayoutStrategyImpl>
void LayoutStrategy<LayoutStrategyImpl>::setParentPoint(widget::Point parentPoint)
{
	m_parentPoint = parentPoint;
}

template<typename LayoutStrategyImpl>
template<layout::ProperLayoutConstraint<LayoutStrategyImpl> LayoutConstraint, unsigned int NumOfConstraints>
std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
LayoutStrategy<LayoutStrategyImpl>::layoutWidgets(const LayoutConstraint (&layoutConstraints)[NumOfConstraints]) const
{
	getImpl().validate(layoutConstraints);
	return getImpl().layoutWidgetsImpl(layoutConstraints);
}

template<typename LayoutStrategyImpl>
template<unsigned int NumOfRows, unsigned int NumOfCols>
consteval LayoutStrategy<LayoutStrategyImpl>::LayoutStrategy(Rows<NumOfRows> numOfRows, Cols<NumOfCols> numOfCols)
	 : m_numOfRows{numOfRows.get()},
		m_numOfCols{numOfCols.get()}
{
	static_assert(NumOfRows != 0 && NumOfCols != 0);
}

template<typename LayoutStrategyImpl>
widget::Size LayoutStrategy<LayoutStrategyImpl>::getParentSize() const
{
	return m_parentSize;
}

template<typename LayoutStrategyImpl>
widget::Point LayoutStrategy<LayoutStrategyImpl>::getParentPoint() const
{
	return m_parentPoint;
}

template<typename LayoutStrategyImpl>
unsigned int LayoutStrategy<LayoutStrategyImpl>::getNumOfRows() const
{
	return m_numOfRows;
}

template<typename LayoutStrategyImpl>
unsigned int LayoutStrategy<LayoutStrategyImpl>::getNumOfCols() const
{
	return m_numOfCols;
}

template<typename LayoutStrategyImpl>
LayoutStrategyImpl& LayoutStrategy<LayoutStrategyImpl>::getImpl()
{
	return static_cast<LayoutStrategyImpl&>(*this);
}

template<typename LayoutStrategyImpl>
const LayoutStrategyImpl& LayoutStrategy<LayoutStrategyImpl>::getImpl() const
{
	return static_cast<const LayoutStrategyImpl&>(*this);
}
}
#endif
