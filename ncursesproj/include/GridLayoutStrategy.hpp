#ifndef GRIDLAYOUTSTRATEGY_HPP
#define GRIDLAYOUTSTRATEGY_HPP
#include "GridLayoutStrategy.fwd.hpp"
#include "GridLayoutStrategyHelpers.hpp"
#include "LayoutStrategy.hpp"

#include <vector>

namespace ncursesplus::layout
{
enum class GridLayoutConstraint
{
	VERTICAL,
	HORIZONTAL,
	NO_CONSTRAINT
};

template<>
struct LayoutStrategyTraits<GridLayoutStrategy>
{
	using LayoutConstraintType = GridLayoutConstraint;
};

class GridLayoutStrategy : public LayoutStrategy<GridLayoutStrategy>
{
public:
	using LayoutStrategy<GridLayoutStrategy>::LayoutConstraintType;
	template<unsigned int NumOfRows, unsigned int NumOfCols>
	consteval GridLayoutStrategy(Rows<NumOfRows>, Cols<NumOfCols>);
private:
	widget::Size computeSize(GridPosition) const;
	widget::Point computePoint(GridPosition) const;
	template<unsigned int NumOfConstraints>
	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
	layoutWidgetsImpl(const LayoutConstraintType (&)[NumOfConstraints]) const;
	template<unsigned int NumOfConstraints>
	void validate(const LayoutConstraintType (&)[NumOfConstraints]) const;
private:
	friend LayoutStrategy<GridLayoutStrategy>;
};

template<unsigned int NumOfRows, unsigned int NumOfCols>
consteval GridLayoutStrategy::GridLayoutStrategy(Rows<NumOfRows> numOfRows, Cols<NumOfCols> numOfCols)
	 : LayoutStrategy{numOfRows, numOfCols}
{}

template<unsigned int NumOfConstraints>
std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
GridLayoutStrategy::layoutWidgetsImpl(const LayoutConstraintType (&layoutConstraints)[NumOfConstraints]) const
{
	CellAvailabilityMatrix cellAvailabilityMatrix(getNumOfRows(), std::vector<bool>(getNumOfCols()));

	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints> widgetLayouts;
	unsigned int i{0u};
	for (const auto& layoutConstraint: layoutConstraints)
	{
		auto gridPosition{computeGridPosition(cellAvailabilityMatrix, layoutConstraint)};

		cellAvailabilityMatrix = registerCells(std::move(cellAvailabilityMatrix), gridPosition);

		auto widgetSize{computeSize(gridPosition)};
		auto widgetPoint{computePoint(gridPosition)};
		widgetLayouts[i] = {widgetSize, widgetPoint};
		i++;
	}
	return widgetLayouts;
}

template<unsigned int NumOfConstraints>
void GridLayoutStrategy::validate(const LayoutConstraintType (&layoutConstraints)[NumOfConstraints]) const
{
	auto numOfRows = getNumOfRows();
	auto numOfCols = getNumOfCols();
	CellAvailabilityMatrix cellAvailabilityMatrix(numOfRows, std::vector<bool>(numOfCols));
	for (const auto& layoutConstraint: layoutConstraints)
	{
		auto gridPosition{computeGridPosition(cellAvailabilityMatrix, layoutConstraint)};
		bool availableCell = cellAvailabilityMatrix[numOfRows - 1][numOfCols - 1];
		assert(availableCell == false);
		cellAvailabilityMatrix = registerCells(std::move(cellAvailabilityMatrix), gridPosition);
	}
}
}
#endif
