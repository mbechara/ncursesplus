#ifndef WIDGETCOMPOSITE_HPP
#define WIDGETCOMPOSITE_HPP
#include "WidgetCommon.hpp"

#include <memory>

namespace ncursesplus::widget
{
class WidgetComposite final : public WidgetCommon
{
public:
	WidgetComposite(const std::string&, Size, Point, customization::WidgetColor&&);
	void add(std::unique_ptr<WidgetCommon>);
	bool handleKeyEvent(event::KeyEvent) override;
	const WidgetCommon* find(std::function<bool(const WidgetCommon*)>) const override;
	void traverse(const std::function<void(const WidgetCommon*)>&) const override;
	bool isActiveWidget() const override;
protected:
	void drawImpl() const override;
private:
	std::vector<std::unique_ptr<WidgetCommon>> m_widgets;
};
}
#endif
