#ifndef TEXTBOXPROPERTIES_HPP
#define TEXTBOXPROPERTIES_HPP
#include <limits>

namespace ncursesplus::widget
{
class TextBoxProperties
{
public:
	void setReplacementChar(char);
	char getReplacementChar() const;
	void setMaxLength(unsigned int);
	unsigned int getMaxLength() const;
private:
	char m_replacementCh{0x00};
	unsigned int m_maxLength{std::numeric_limits<unsigned int>::max()};
};
}
#endif
