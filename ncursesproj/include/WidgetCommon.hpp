#ifndef WIDGETCOMMON_HPP
#define WIDGETCOMMON_HPP
#include "KeyEventObserver.hpp"
#include "Point.fwd.hpp"
#include "Size.fwd.hpp"
#include "WidgetColor.hpp"
#include "Window.fwd.hpp"

#include <functional>
#include <memory>
#include <string>

namespace ncursesplus::widget
{
class WidgetCommon : public event::KeyEventObserver
{
public:
	WidgetCommon(const std::string&, Size, Point, customization::WidgetColor&&);
	virtual ~WidgetCommon();
	WidgetCommon(const WidgetCommon&) = delete;
	void operator=(const WidgetCommon&) = delete;
	std::string_view getName() const;
	Size getSize() const;
	Point getPoint() const;
	void draw() const;
	virtual const WidgetCommon* find(std::function<bool(const WidgetCommon*)>) const;
	virtual void traverse(const std::function<void(const WidgetCommon*)>&) const;
	virtual bool isActiveWidget() const = 0;
protected:
	virtual void drawImpl() const = 0;
protected:
	std::string m_name;
	std::unique_ptr<window::Window> m_window;
};
}
#endif
