#ifndef TEXTBOXWIDGETBUILDER_HPP
#define TEXTBOXWIDGETBUILDER_HPP
#include "TextBoxProperties.hpp"
#include "WidgetBuilder.hpp"

namespace ncursesplus::widget::builder
{
class TextBoxWidgetBuilder final : public WidgetBuilder<TextBoxWidgetBuilder>
{
public:
	TextBoxWidgetBuilder(const std::string&);
	TextBoxWidgetBuilder& withScroll();
	TextBoxWidgetBuilder& withCharReplacement(char);
	TextBoxWidgetBuilder& withMaxLength(unsigned int);
	std::unique_ptr<Widget> buildImpl() override;
private:
	bool m_scroll{false};
	TextBoxProperties m_textBoxProperties;
};
}
#endif
