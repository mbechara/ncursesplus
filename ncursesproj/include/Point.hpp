#ifndef POINT_HPP
#define POINT_HPP
#include "PhantomType.hpp"
#include "Point.fwd.hpp"

namespace ncursesplus::widget
{
using Y = PhantomType<unsigned int, struct YParameter>;
using X = PhantomType<unsigned int, struct XParameter>;

namespace literals
{
consteval Y operator""_y(unsigned long long int y)
{
	return Y(y);
}

consteval X operator"" _x(unsigned long long int x)
{
	return X(x);
}
}

class Point
{
public:
	constexpr Point() = default;
	constexpr Point(Y, X);
	unsigned int getY() const;
	unsigned int getX() const;
	bool operator==(const Point&) const = default;
	Point& operator+=(const Point&);
	Point& operator-=(const Point&);
	Point& operator*=(const Point&);
	Point& operator/=(const Point&);
private:
	unsigned int m_y{0};
	unsigned int m_x{0};
};

constexpr Point::Point(Y y, X x)
	 : m_y{y.get()},
		m_x{x.get()}
{}

Point operator+(const Point&, const Point&);
Point operator-(const Point&, const Point&);
Point operator*(const Point&, const Point&);
Point& operator*=(Point&, const Point&);
Point operator/(const Point&, const Point&);
Point& operator/=(Point&, const Point&);
}
#endif
