#ifndef PHANTOMTYPE_HPP
#define PHANTOMTYPE_HPP

template<typename T, typename PhantomParameter>
class PhantomType
{
public:
	constexpr explicit PhantomType(T value);
	constexpr T get() const;
private:
	T m_value;
};

template<typename T, typename PhantomParameter>
constexpr PhantomType<T, PhantomParameter>::PhantomType(T value)
	 : m_value{value}
{}

template<typename T, typename PhantomParameter>
constexpr T PhantomType<T, PhantomParameter>::get() const
{
	return m_value;
}

template<typename T, typename PhantomParameter, T Value = T{}>
class PhantomValue
{
public:
	constexpr T get() const;
};

template<typename T, typename PhantomParameter, T Value>
constexpr T PhantomValue<T, PhantomParameter, Value>::get() const
{
	return Value;
}

#endif
