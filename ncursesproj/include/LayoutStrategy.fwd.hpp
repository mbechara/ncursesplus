#ifndef LAYOUTSTRATEGY_FWD_HPP
#define LAYOUTSTRATEGY_FWD_HPP
#include <type_traits>

namespace ncursesplus::layout
{
template<typename LayoutStrategyImpl>
class LayoutStrategy;

template<typename LayoutStrategyImpl>
struct LayoutStrategyTraits;

template<typename LayoutConstraint, typename LayoutStrategyImpl>
concept ProperLayoutConstraint = std::is_same_v<LayoutConstraint, typename LayoutStrategyImpl::LayoutConstraintType>;

template<typename LayoutStrategyImpl>
concept IsLayoutStrategy = std::is_base_of_v<LayoutStrategy<LayoutStrategyImpl>, LayoutStrategyImpl>;
}
#endif
