#ifndef NEIGHBORWIDGETSMEDIATOR_HPP
#define NEIGHBORWIDGETSMEDIATOR_HPP
#include "Mediator.hpp"

namespace ncursesplus::widget
{
class NeighborWidgetsMediator : public Mediator
{
public:
	void notify(const Widget* sender, const std::string& side) override;
};
}
#endif
