#ifndef WIDGETCOLOR_HPP
#define WIDGETCOLOR_HPP
#include "Helpers.hpp"
#include "PhantomType.hpp"
#include "Window.fwd.hpp"
#include <algorithm>
#include <array>

namespace ncursesplus::widget::customization
{
enum class Color
{
	DEFAULT,
	BLACK,
	RED,
	GREEN,
	YELLOW,
	BLUE,
	MAGENTA,
	CYAN,
	WHITE,
	COUNT
};

constexpr int ColorsCount{static_cast<int>(Color::COUNT)};

using BackgroundColor = PhantomType<Color, struct BackgroundColorParameter>;
using ForegroundColor = PhantomType<Color, struct ForegroundColorParameter>;

class WidgetColor
{
public:
	consteval WidgetColor(ForegroundColor, BackgroundColor);
	consteval WidgetColor();
	void apply(const window::Window*) const;
private:
	Color m_foregroundColor;
	Color m_backgroundColor;
	short m_pairIndex{};
};

consteval auto generateColorCombinations()
{
	constexpr auto colorCombinationCount = pow(ColorsCount, 2);
	std::array<std::pair<Color, Color>, colorCombinationCount> colorCombinations;
	unsigned int index{};
	for (auto i{0u}; i < ColorsCount; i++)
	{
		auto foregroundColor{static_cast<Color>(i)};
		for (auto j{0u}; j < ColorsCount; j++)
		{
			auto backgroundColor{static_cast<Color>(j)};
			colorCombinations[index] = {foregroundColor, backgroundColor};
			index++;
		}
	}
	return colorCombinations;
}

consteval WidgetColor::WidgetColor(ForegroundColor foregroundColor, BackgroundColor backgroundColor)
	 : m_foregroundColor{foregroundColor.get()},
		m_backgroundColor{backgroundColor.get()}
{
	constexpr auto colorCombinations{generateColorCombinations()};
	auto colorCombination{std::ranges::find(colorCombinations, std::make_pair(m_foregroundColor, m_backgroundColor))};
	m_pairIndex = std::distance(begin(colorCombinations), colorCombination);
}

consteval WidgetColor::WidgetColor()
	 : WidgetColor{ForegroundColor{Color::DEFAULT}, BackgroundColor{Color::DEFAULT}}
{}
}
#endif
