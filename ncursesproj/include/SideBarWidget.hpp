#ifndef SIDEBARWIDGET_HPP
#define SIDEBARWIDGET_HPP
#include "Helpers.hpp"
#include "Point.hpp"
#include "Size.hpp"
#include "Widget.hpp"
#include "Window.hpp"
#include <cmath>

namespace ncursesplus::widget
{
template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
class SideBarWidget final : public Widget
{
	static_assert(MenuEntryPercentage != 0u && MenuEntryPercentage % 10 == 0 &&
					  NumMenuEntries * MenuEntryPercentage <= 100u && NumMenuEntries >= 1u);
public:
	SideBarWidget(const std::string&, Size, Point, customization::WidgetColor&&);
	template<AreStrings... T>
	void addMenuEntries(const T&...);
	std::string getText() const;
	Point getCursor() const override;
	Size getMenuEntrySize() const;
	Point getMenuEntryPoint(const std::string&) const;
private:
	void handleKeyEventImpl(event::KeyEvent) override;
	void moveCursorImpl() const override;
	void drawImpl() const override;
private:
	using MenuEntry = std::pair<window::Window, std::string>;
	std::array<MenuEntry, NumMenuEntries> m_menuEntries;
	unsigned int m_currentMenuEntryIndex{};
	Size m_menuEntrySize;
};

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
SideBarWidget<NumMenuEntries, MenuEntryPercentage>::SideBarWidget(const std::string& name, Size size, Point point,
																						customization::WidgetColor&& widgetColor)
	 : Widget{name, size, point, std::move(widgetColor)},
		m_menuEntrySize{Height{static_cast<unsigned int>(std::ceil(size.getHeight() * (MenuEntryPercentage / 100.f)))},
							 Width{size.getWidth()}}
{}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
template<AreStrings... T>
void SideBarWidget<NumMenuEntries, MenuEntryPercentage>::addMenuEntries(const T&... menuEntries)
{
	static_assert(sizeof...(menuEntries) == NumMenuEntries);
	unsigned int i{0u};
	for (const auto& menuEntry: {menuEntries...})
	{
		auto sideBarPoint{getPoint()};
		Point menuEntryPoint{Y{i * m_menuEntrySize.getHeight() + sideBarPoint.getY() - i}, X{sideBarPoint.getX()}};

		window::Window menuEntryWindow{m_menuEntrySize, menuEntryPoint};

		m_menuEntries[i] = {menuEntryWindow, menuEntry};
		i++;
	}
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
std::string SideBarWidget<NumMenuEntries, MenuEntryPercentage>::getText() const
{
	const auto& [menuEntryWindow, menuEntryName]{m_menuEntries[m_currentMenuEntryIndex]};
	return menuEntryName;
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
Point SideBarWidget<NumMenuEntries, MenuEntryPercentage>::getCursor() const
{
	using namespace literals;
	return Point{0_y, 0_x};
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
Size SideBarWidget<NumMenuEntries, MenuEntryPercentage>::getMenuEntrySize() const
{
	return m_menuEntrySize;
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
Point SideBarWidget<NumMenuEntries, MenuEntryPercentage>::getMenuEntryPoint(const std::string& menuEntryName) const
{
	auto menuEntry{std::ranges::find_if(m_menuEntries, [&menuEntryName](const auto& menuEntry) {
		return menuEntry.second == menuEntryName;
	})};
	return menuEntry->first.getPoint();
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
void SideBarWidget<NumMenuEntries, MenuEntryPercentage>::handleKeyEventImpl(event::KeyEvent keyEvent)
{
	auto key{keyEvent.getKey()};
	switch (key)
	{
		case KEY_DOWN:
			if (m_currentMenuEntryIndex < NumMenuEntries - 1)
				m_currentMenuEntryIndex++;
			break;
		case KEY_UP:
			if (m_currentMenuEntryIndex > 0u)
				m_currentMenuEntryIndex--;
	}
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
void SideBarWidget<NumMenuEntries, MenuEntryPercentage>::moveCursorImpl() const
{
	curs_set(0);
	const auto& [menuEntry, menuEntryName] = m_menuEntries[m_currentMenuEntryIndex];
	auto* menuEntryNcursesWindow{menuEntry.getNcursesWindow()};
	wmove(menuEntryNcursesWindow, 0, 0);
	wrefresh(menuEntryNcursesWindow);
	/*
	start_color();
	init_pair(1, COLOR_BLACK, COLOR_WHITE);
	wbkgd(menuEntry.getNcursesWindow(), COLOR_PAIR(1));
	*/
}

template<unsigned int NumMenuEntries, unsigned int MenuEntryPercentage>
void SideBarWidget<NumMenuEntries, MenuEntryPercentage>::drawImpl() const
{
	for (const auto& [menuEntryWindow, menuEntryName]: m_menuEntries)
	{
		auto menuEntrySize{menuEntryWindow.getSize()};
		unsigned int xOffset{menuEntrySize.getWidth() / 2u - static_cast<unsigned int>(menuEntryName.size()) / 2u};
		unsigned int yOffset{menuEntrySize.getHeight() / 2u};
		auto* menuEntryNcursesWindow{menuEntryWindow.getNcursesWindow()};
		mvwaddstr(menuEntryNcursesWindow, yOffset, xOffset, menuEntryName.c_str());
		box(menuEntryNcursesWindow, 0, 0);
		wrefresh(menuEntryNcursesWindow);
	}
}
}
#endif
