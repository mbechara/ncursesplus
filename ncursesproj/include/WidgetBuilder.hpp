#ifndef WIDGETBUILDER_HPP
#define WIDGETBUILDER_HPP
#include "Mediator.hpp"
#include "Widget.hpp"
#include "WidgetBuilderCommon.hpp"

#include <memory>

namespace ncursesplus::widget::builder
{
template<typename WidgetBuilderImpl>
class WidgetBuilder : public WidgetBuilderCommon
{
public:
	WidgetBuilder(const std::string&);
	WidgetBuilderImpl& withMediator(std::shared_ptr<Mediator> mediator);
	WidgetBuilderImpl& asActive();
	std::unique_ptr<WidgetCommon> build() override final;
protected:
	std::shared_ptr<Mediator> m_mediator;
private:
	unsigned int getNumOfActiveWidgets() const override final;
	virtual std::unique_ptr<Widget> buildImpl() = 0;
private:
	bool m_activeWidget{false};
};

template<typename WidgetBuilderImpl>
WidgetBuilder<WidgetBuilderImpl>::WidgetBuilder(const std::string& name)
	 : WidgetBuilderCommon{name}
{}

template<typename WidgetBuilderImpl>
WidgetBuilderImpl& WidgetBuilder<WidgetBuilderImpl>::withMediator(std::shared_ptr<Mediator> mediator)
{
	auto& widgetBuilderImpl{static_cast<WidgetBuilderImpl&>(*this)};
	widgetBuilderImpl.m_mediator = mediator;
	return static_cast<WidgetBuilderImpl&>(*this);
}

template<typename WidgetBuilderImpl>
WidgetBuilderImpl& WidgetBuilder<WidgetBuilderImpl>::asActive()
{
	m_activeWidget = true;
	return static_cast<WidgetBuilderImpl&>(*this);
}

template<typename WidgetBuilderImpl>
std::unique_ptr<WidgetCommon> WidgetBuilder<WidgetBuilderImpl>::build()
{
	auto widget{buildImpl()};
	if (m_activeWidget)
		widget->moveCursor();
	widget->addMediator(m_mediator);
	return widget;
}

template<typename WidgetBuilderImpl>
unsigned int WidgetBuilder<WidgetBuilderImpl>::getNumOfActiveWidgets() const
{
	return m_activeWidget;
}
}
#endif
