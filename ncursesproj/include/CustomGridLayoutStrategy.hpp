#ifndef CUSTOMGRIDLAYOUTSTRATEGY_HPP
#define CUSTOMGRIDLAYOUTSTRATEGY_HPP
#include "Helpers.hpp"
#include "LayoutStrategy.hpp"
#include <span>

namespace ncursesplus::layout
{
struct CustomGridLayoutConstraint
{
	consteval CustomGridLayoutConstraint(unsigned int rowStart, unsigned int rowEnd, unsigned int colStart,
													 unsigned int colEnd)
		 : m_rowStart{rowStart},
			m_rowEnd{rowEnd},
			m_colStart{colStart},
			m_colEnd{colEnd}
	{}

	consteval CustomGridLayoutConstraint() = default;

	unsigned int m_rowStart;
	unsigned int m_rowEnd;
	unsigned int m_colStart;
	unsigned int m_colEnd;
};

class CustomGridLayoutStrategy;

template<>
struct LayoutStrategyTraits<CustomGridLayoutStrategy>
{
	using LayoutConstraintType = CustomGridLayoutConstraint;
};

class CustomGridLayoutStrategy : public LayoutStrategy<CustomGridLayoutStrategy>
{
public:
	using LayoutStrategy<CustomGridLayoutStrategy>::LayoutConstraintType;
	template<unsigned int NumOfRows, unsigned int NumOfCols>
	consteval CustomGridLayoutStrategy(Rows<NumOfRows>, Cols<NumOfCols>);
private:
	widget::Size computeSize(LayoutConstraintType) const;
	widget::Point computePoint(LayoutConstraintType) const;
	template<unsigned int NumOfConstraints>
	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
	layoutWidgetsImpl(const LayoutConstraintType (&)[NumOfConstraints]) const;
	template<unsigned int NumOfConstraints>
	void validate(const LayoutConstraintType (&)[NumOfConstraints]) const;
	CustomGridLayoutConstraint getLayoutConstraint(const std::string& widgetName) const;
private:
	friend LayoutStrategy<CustomGridLayoutStrategy>;
};

template<unsigned int NumOfRows, unsigned int NumOfCols>
consteval CustomGridLayoutStrategy::CustomGridLayoutStrategy(Rows<NumOfRows> numOfRows, Cols<NumOfCols> numOfCols)
	 : LayoutStrategy{numOfRows, numOfCols}
{}

template<unsigned int NumOfConstraints>
std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
CustomGridLayoutStrategy::layoutWidgetsImpl(const LayoutConstraintType (&layoutConstraints)[NumOfConstraints]) const
{
	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints> widgetLayouts;
	unsigned int i{0u};
	for (const auto& layoutConstraint: layoutConstraints)
	{
		auto widgetSize{computeSize(layoutConstraint)};
		auto widgetPoint{computePoint(layoutConstraint)};
		widgetLayouts[i] = {widgetSize, widgetPoint};
		i++;
	}
	return widgetLayouts;
}

template<unsigned int NumOfConstraints>
void CustomGridLayoutStrategy::validate(const LayoutConstraintType (&layoutConstraints)[NumOfConstraints]) const
{
	for (auto i{0u}; i < NumOfConstraints; i++)
	{
		auto layoutConstraint{layoutConstraints[i]};
		assert(layoutConstraint.m_rowEnd <= getNumOfRows() && layoutConstraint.m_colEnd <= getNumOfCols());
		assert(layoutConstraint.m_rowEnd > layoutConstraint.m_rowStart &&
				 layoutConstraint.m_colEnd > layoutConstraint.m_colStart);

		auto overlapCurrentLayoutConstraint = [&layoutConstraint](const auto& otherLayoutConstraint) {
			return intersectionArea(layoutConstraint, otherLayoutConstraint) > 0;
		};

		std::span otherLayoutContraints{std::span{layoutConstraints}.subspan(i + 1, NumOfConstraints - 1)};
		assert(std::ranges::none_of(otherLayoutContraints, overlapCurrentLayoutConstraint));
	}
}
}
#endif
