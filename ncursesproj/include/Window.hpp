#ifndef WINDOW_HPP
#define WINDOW_HPP
#include "Point.hpp"
#include "Size.hpp"
#include "Window.fwd.hpp"

#include <ncurses.h>

namespace ncursesplus::window
{
class Window
{
public:
	Window() = default;
	Window(widget::Size, widget::Point);
	widget::Size getSize() const;
	widget::Point getPoint() const;
	WINDOW* getNcursesWindow() const;
private:
	WINDOW* m_ncursesWindow{};
	widget::Size m_size;
	widget::Point m_point;
};
}
#endif
