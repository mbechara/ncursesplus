#ifndef WIDGET_HPP
#define WIDGET_HPP
#include "Mediator.fwd.hpp"
#include "Widget.fwd.hpp"
#include "WidgetCommon.hpp"

#include <ncurses.h>

namespace ncursesplus::widget
{
class Widget : public WidgetCommon
{
public:
	Widget(const std::string&, Size, Point, customization::WidgetColor&&);
	virtual ~Widget() = default;
	bool isActiveWidget() const override;
	void addMediator(std::shared_ptr<Mediator>);
	int read() const;
	bool handleKeyEvent(event::KeyEvent) override;
	void moveCursor() const;
	virtual Point getCursor() const = 0;
private:
	virtual int readImpl() const;
	virtual void moveCursorImpl() const = 0;
	virtual void handleKeyEventImpl(event::KeyEvent) = 0;
private:
	static std::string m_activeWidget;
	std::shared_ptr<Mediator> m_mediator{nullptr};
};

const WidgetCommon* find(const WidgetCommon*, const std::string&);
const Widget* findActiveWidget(const WidgetCommon*);
int readInput(const WidgetCommon*);
}
#endif
