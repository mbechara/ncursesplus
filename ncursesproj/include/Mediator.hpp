#ifndef MEDIATOR_HPP
#define MEDIATOR_HPP
#include "Mediator.fwd.hpp"
#include "Widget.fwd.hpp"
#include <string_view>
#include <vector>

namespace ncursesplus::widget
{
class Mediator
{
public:
	virtual ~Mediator() = default;
	void add(Widget*);
	virtual void notify(const Widget* sender, const std::string& message) = 0;
protected:
	std::vector<Widget*> m_widgets;
};
}

#endif
