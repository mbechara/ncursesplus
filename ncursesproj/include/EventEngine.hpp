#ifndef EVENTENGINE_HPP
#define EVENTENGINE_HPP
#include "WidgetCommon.fwd.hpp"

namespace ncursesplus::event
{
class EventEngine
{
public:
	void launch(widget::WidgetCommon&);
};
}
#endif
