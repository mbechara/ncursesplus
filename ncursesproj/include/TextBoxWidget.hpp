#ifndef TEXTBOXWIDGET_HPP
#define TEXTBOXWIDGET_HPP
#include "TextManipulator.hpp"
#include "Widget.hpp"

namespace ncursesplus::widget
{
template<ScrollMechanism scrollMechanism>
class TextBoxWidget final : public Widget
{
public:
	TextBoxWidget(const std::string&, Size, Point, TextBoxProperties&&, customization::WidgetColor&&);
	~TextBoxWidget();
	std::string getText() const;
	Point getCursor() const override;
private:
	void handleKeyEventImpl(event::KeyEvent) override;
	void moveCursorImpl() const override;
	int readImpl() const override;
	void drawImpl() const override;
	void updateText() const;
	void addText(const std::string&) const;
	void clearText() const;
private:
	TextManipulator<scrollMechanism> m_textManipulator;
	std::unique_ptr<window::Window> m_innerWindow;
};
}
#endif
