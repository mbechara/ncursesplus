#ifndef SIZE_HPP
#define SIZE_HPP
#include "PhantomType.hpp"
#include "Size.fwd.hpp"

namespace ncursesplus::widget
{
using Height = PhantomType<unsigned int, struct HeightParameter>;
using Width = PhantomType<unsigned int, struct WidthParameter>;

namespace literals
{
consteval Height operator""_h(unsigned long long int lines)
{
	return Height(lines);
}

consteval Width operator""_w(unsigned long long int cols)
{
	return Width(cols);
}
}

class Size
{
public:
	constexpr Size() = default;
	constexpr Size(Height, Width);
	unsigned int getHeight() const;
	unsigned int getWidth() const;
	bool operator==(const Size&) const = default;
	Size& operator+=(const Size&);
	Size& operator-=(const Size&);
	Size& operator*=(const Size&);
	Size& operator/=(const Size&);
private:
	unsigned int m_height{0u};
	unsigned int m_width{0u};
};

constexpr Size::Size(Height height, Width width)
	 : m_height{height.get()},
		m_width{width.get()}
{}

Size operator+(const Size&, const Size&);
Size operator-(const Size&, const Size&);
Size operator*(const Size&, const Size&);
Size& operator*=(Size&, const Size&);
Size operator/(const Size&, const Size&);
Size& operator/=(Size&, const Size&);
}
#endif
