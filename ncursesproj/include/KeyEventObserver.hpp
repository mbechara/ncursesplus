#ifndef KEYEVENTOBSERVER_HPP
#define KEYEVENTOBSERVER_HPP

namespace ncursesplus::event
{
class KeyEvent
{
public:
	KeyEvent(int);
	int getKey() const;
	bool isKeyCombination() const;
private:
	int m_key;
};

class KeyEventObserver
{
public:
	virtual bool handleKeyEvent(KeyEvent) = 0;
};
}
#endif
