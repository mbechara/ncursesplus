#ifndef GRIDLAYOUTSTRATEGY_FWD_HPP
#define GRIDLAYOUTSTRATEGY_FWD_HPP
#include <type_traits>

namespace ncursesplus::layout
{
class GridLayoutStrategy;
enum class GridLayoutConstraint;

template<typename T>
concept IsGridLayoutConstraint = std::is_same_v<T, GridLayoutConstraint>;
}
#endif
