#ifndef SESSION_HPP
#define SESSION_HPP

namespace ncursesplus
{
class Session
{
public:
	Session();
	~Session();
	Session& withColor();
};
}
#endif
