#ifndef WIDGETCOMPOSITEBUILDER_HPP
#define WIDGETCOMPOSITEBUILDER_HPP
#include "LayoutStrategy.fwd.hpp"
#include "WidgetBuilderCommon.hpp"
#include "WidgetComposite.hpp"

namespace ncursesplus::widget::builder
{
template<unsigned int NumOfWidgetsValue>
using NumOfWidgets = PhantomValue<unsigned int, struct NumOfWidgetsParameters, NumOfWidgetsValue>;

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
class WidgetCompositeBuilder final : public WidgetBuilderCommon
{
public:
	WidgetCompositeBuilder(const std::string&, NumOfWidgets<NumOfWidgetsValue>, LayoutStrategyImpl);
	template<typename WidgetBuilder, layout::ProperLayoutConstraint<LayoutStrategyImpl> LayoutConstraint>
	WidgetCompositeBuilder& add(WidgetBuilder, LayoutConstraint);
	WidgetCompositeBuilder& withSize(Size);
	WidgetCompositeBuilder& withPoint(Point);
	std::unique_ptr<WidgetCommon> build() override;
private:
	unsigned int getNumOfActiveWidgets() const override;
private:
	LayoutStrategyImpl m_layoutStrategy;
	typename LayoutStrategyImpl::LayoutConstraintType m_layoutConstraints[NumOfWidgetsValue];
	std::array<std::shared_ptr<WidgetBuilderCommon>, NumOfWidgetsValue> m_widgetBuilders;
	unsigned int m_currentIndex{};
};

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
WidgetCompositeBuilder(std::string, NumOfWidgets<NumOfWidgetsValue>, LayoutStrategyImpl)
	-> WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>;

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>::WidgetCompositeBuilder(const std::string& name,
																												  NumOfWidgets<NumOfWidgetsValue>,
																												  LayoutStrategyImpl layoutStrategy)
	 : WidgetBuilderCommon{name},
		m_layoutStrategy{std::move(layoutStrategy)}
{}

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
template<typename WidgetBuilder, layout::ProperLayoutConstraint<LayoutStrategyImpl> LayoutConstraint>
WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>&
WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>::add(WidgetBuilder widgetBuilder,
																						 LayoutConstraint layoutConstraint)
{
	m_layoutConstraints[m_currentIndex] = layoutConstraint;
	m_widgetBuilders[m_currentIndex] = std::make_shared<WidgetBuilder>(widgetBuilder);
	m_currentIndex++;
	return *this;
}

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>&
WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>::withSize(Size size)
{
	m_size = size;
	m_layoutStrategy.setParentSize(m_size);
	return *this;
}

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>&
WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>::withPoint(Point point)
{
	m_point = point;
	m_layoutStrategy.setParentPoint(m_point);
	return *this;
}

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
std::unique_ptr<WidgetCommon> WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>::build()
{
	auto numOfActiveWidget{getNumOfActiveWidgets()};
	assert(numOfActiveWidget <= 1);

	auto widgetComposite{std::make_unique<WidgetComposite>(m_name, m_size, m_point, customization::WidgetColor{})};

	auto widgetLayouts{m_layoutStrategy.layoutWidgets(m_layoutConstraints)};

	for (auto i{0u}; i < NumOfWidgetsValue; i++)
	{
		const auto& [size, point]{widgetLayouts[i]};
		m_widgetBuilders[i]->m_size = size;
		m_widgetBuilders[i]->m_point = point;
		auto widget{m_widgetBuilders[i]->build()};
		widgetComposite->add(std::move(widget));
	}
	return widgetComposite;
}

template<unsigned int NumOfWidgetsValue, layout::IsLayoutStrategy LayoutStrategyImpl>
unsigned int WidgetCompositeBuilder<NumOfWidgetsValue, LayoutStrategyImpl>::getNumOfActiveWidgets() const
{
	unsigned int numOfActiveWidgets{0u};
	for (auto widgetBuilder: m_widgetBuilders)
		numOfActiveWidgets += widgetBuilder->getNumOfActiveWidgets();
	return numOfActiveWidgets;
}
}
#endif
