sp := $(sp).x
dirstack_$(sp) := $(d)
d := $(dir)

dir := $(d)/test
include $(dir)/Rules.mk

SRCS_$(d) := $(wildcard $(d)/$(SRC_DIR)/*.cpp)

include common/release/Artifacts.mk
include common/debug/Artifacts.mk

TGT_LIB_RELEASE_$(d) := $(d)/$(RELEASE_DIR)/$(LIB_DIR)/libncursesplus.a

$(TGT_LIB_RELEASE_$(d)): CPPFLAGS := -I$(d)/$(INC_DIR)
$(TGT_LIB_RELEASE_$(d)): CXXFLAGS += -O3

TGTS_LIB_RELEASE := $(TGTS_LIB_RELEASE) $(TGT_LIB_RELEASE_$(d))

TGT_LIB_DEBUG_$(d) := $(d)/$(DEBUG_DIR)/$(LIB_DIR)/libncursesplus.a

$(TGT_LIB_DEBUG_$(d)): CPPFLAGS := -I$(d)/$(INC_DIR)
$(TGT_LIB_DEBUG_$(d)): CXXFLAGS += -g

TGTS_LIB_DEBUG := $(TGTS_LIB_DEBUG) $(TGT_LIB_DEBUG_$(d))

include common/release/LibRules.mk
include common/debug/LibRules.mk

CLEAN := $(CLEAN) $(d)/$(BUILD_DIR)

d := $(dirstack_$(sp))
sp := $(basename $(sp))
