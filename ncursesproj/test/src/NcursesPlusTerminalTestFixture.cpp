#include "NcursesPlusTerminalTestFixture.hpp"

namespace ncursesplus::test
{
NcursesPlusTerminalTestFixture::NcursesPlusTerminalTestFixture(int height, int width)
{
	char fileName[] = "/tmp/nptestXXXXXX";
	int fd = mkstemp(fileName);
	fileHandle = fdopen(fd, "w+");
	screen = newterm(nullptr, fileHandle, fileHandle);
	resizeterm_sp(screen, height, width);
}

NcursesPlusTerminalTestFixture::~NcursesPlusTerminalTestFixture()
{
	endwin_sp(screen);
	fclose(fileHandle);
}
}
