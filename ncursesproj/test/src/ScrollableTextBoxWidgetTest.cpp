#include "NcursesPlusTerminalTestFixture.hpp"
#include "Size.hpp"
#include "TextBoxWidget.hpp"
#include <gtest/gtest.h>

using namespace ncursesplus;
using namespace widget::literals;

class ScrollableTextBoxWidgetTestFixture : public test::NcursesPlusTerminalTestFixture
{
protected:
	ScrollableTextBoxWidgetTestFixture()
		 : test::NcursesPlusTerminalTestFixture{50, 50},
			m_textBoxWidget{"TextBoxWidget", widget::Size{5_h, 20_w}, widget::Point{0_y, 0_x}, widget::TextBoxProperties{},
								 widget::customization::WidgetColor{}}
	{
		m_textBoxWidget.moveCursor();
	}
protected:
	widget::TextBoxWidget<widget::ScrollMechanism::WITH_SCROLL> m_textBoxWidget;
};

constexpr std::string_view ncursesplusText{"ncursesplus"};
constexpr std::string_view ncursesplusLongText{"ncursesplus-ncursesplus"};

TEST_F(ScrollableTextBoxWidgetTestFixture, text_box_widget_properly_return_text)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture, text_box_widget_properly_return_text_spanning_on_two_lines)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusLongText);
}

TEST_F(
	ScrollableTextBoxWidgetTestFixture,
	insert_character_after_third_line_end_col_adds_text_on_fourth_line_and_moves_cursor_position_to_begin_col_of_fourth_line)
{
	std::string ncursesplusLongText{"ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncurse"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusLongText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'s'});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncurses");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 1_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 insert_newline_at_third_line_moves_cursor_position_to_begin_col_of_fourth_line_and_adds_text_to_fourth_line)
{
	std::string ncursesplusLongText{"ncursesplus-ncursesplus-ncursesplus-ncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 11_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 0_x}));

	for (auto ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncursesplus-ncursesplus-ncursesplus\nncursesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 11_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 text_properly_inserted_and_cursor_at_correct_position_after_passing_third_line_and_inserting_newline_characters)
{
	std::string ncursesplusLongText{"ncursesplus\rncursesplus-ncursesplus\rncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus\nncursesplus-ncursesplus\nncursesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 11_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture, up_arrow_on_fourth_line_moves_cursor_position_to_third_line)
{
	std::string ncursesplusLongText{"ncursesplus\rncursesplus-ncursesplus\rncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture, right_arrow_all_the_way_on_third_line_moves_cursor_position_to_fourth_line)
{
	std::string ncursesplusLongText{"ncursesplus\rncursesplus-ncursesplus\rncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	for (auto i{1u}; i <= 10; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 9_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture, up_arrow_all_the_way_from_fourth_line_reaches_first_line)
{
	std::string ncursesplusLongText{"ncursesplus\rncursesplus-ncursesplus\rncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 5_x}));

	for (auto i{1u}; i <= 10; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 15_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture, down_arrow_all_the_way_from_first_line_reaches_fifth_line)
{
	std::string ncursesplusLongText{"ncursesplus\rncursesplus-ncursesplus\rncursesplus\rncurses"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus\nncursesplus-ncursesplus\nncursesplus\nncurses");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 5_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));

	for (auto i{1u}; i <= 3; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 5_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 5_x}));

	for (auto i{1u}; i <= 3; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 8_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 7_x}));
}

TEST_F(ScrollableTextBoxWidgetTestFixture, left_arrow_all_the_way_from_fourth_line_moves_cursor_position_to_first_line)
{
	std::string ncursesplusLongText{"ncursesplus\rncursesplus-ncursesplus\rncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 12; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));

	for (auto i{1u}; i <= 6; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 17_x}));

	for (auto i{1u}; i <= 18; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

namespace
{
auto getTextManipulator(std::string text)
{
	widget::TextManipulator<widget::ScrollMechanism::WITH_SCROLL> textManipulator{widget::Size{3_h, 18_w}};
	textManipulator.setCursorPos(widget::Point{0_y, 0_x});

	for (auto ch: text)
	{
		textManipulator.addCh(ch);
		textManipulator.moveCursorPos(widget::RightPos{});
	}

	return textManipulator;
}
}

TEST_F(ScrollableTextBoxWidgetTestFixture, text_manipulator_displays_text_first_three_lines)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus"};

	auto textManipulator{getTextManipulator(ncursesplusLongText)};

	ASSERT_EQ(textManipulator.getDisplayedText(), ncursesplusLongText);
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 inserting_text_past_third_line_on_text_manipulator_displays_text_from_second_to_fourth_line)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	auto textManipulator{getTextManipulator(ncursesplusLongText)};

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus-ncursesplus\nncursesplus");
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 up_arrow_all_the_way_to_first_line_on_text_manipulator_displays_text_first_three_lines)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	auto textManipulator{getTextManipulator(ncursesplusLongText)};

	textManipulator.moveCursorPos(widget::UpPos{});
	textManipulator.moveCursorPos(widget::UpPos{});

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus-ncursesplus\nncursesplus");

	textManipulator.moveCursorPos(widget::UpPos{});

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus\nncursesplus-ncursesplus\n");
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 down_arrow_all_the_way_to_fourth_line_on_text_manipulator_displays_text_from_second_to_fourth_line)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	auto textManipulator{getTextManipulator(ncursesplusLongText)};

	textManipulator.moveCursorPos(widget::UpPos{});
	textManipulator.moveCursorPos(widget::UpPos{});
	textManipulator.moveCursorPos(widget::UpPos{});

	textManipulator.moveCursorPos(widget::DownPos{});
	textManipulator.moveCursorPos(widget::DownPos{});

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus\nncursesplus-ncursesplus\n");

	textManipulator.moveCursorPos(widget::DownPos{});

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus-ncursesplus\nncursesplus");
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 left_arrow_all_the_way_to_first_line_on_text_manipulator_displays_text_first_three_lines)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	auto textManipulator{getTextManipulator(ncursesplusLongText)};

	for (auto i{1u}; i <= 36; i++)
		textManipulator.moveCursorPos(widget::LeftPos{});

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus\nncursesplus-ncursesplus\n");
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 backspace_all_the_way_to_first_line_on_text_manipulator_displays_text_first_line)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	auto textManipulator{getTextManipulator(ncursesplusLongText)};

	for (auto i{1u}; i <= 36; i++)
	{
		textManipulator.removeCh(widget::BackwardsRemove{});
		textManipulator.moveCursorPos(widget::LeftPos{});
	}

	ASSERT_EQ(textManipulator.getDisplayedText(), ncursesplusText);
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 del_the_first_line_on_text_manipulator_will_shift_fourth_line_up_and_display_first_three_lines)
{
	std::string ncursesPlusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	auto textManipulator{getTextManipulator(ncursesPlusLongText)};

	textManipulator.moveCursorPos(widget::UpPos{});
	textManipulator.moveCursorPos(widget::UpPos{});
	textManipulator.moveCursorPos(widget::UpPos{});

	for (auto i{1u}; i <= 5; i++)
		textManipulator.moveCursorPos(widget::LeftPos{});

	ASSERT_EQ(textManipulator.getCursorPos(), widget::Point(0_y, 0_x));

	for (auto i{1u}; i <= 12; i++)
		textManipulator.removeCh(widget::ForwardsRemove{});

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus-ncursesplus\nncursesplus");
}

TEST_F(ScrollableTextBoxWidgetTestFixture,
		 del_entire_fourth_line_on_text_manipulator_will_display_second_and_third_line)
{
	std::string ncursesPlusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	auto textManipulator{getTextManipulator(ncursesPlusLongText)};

	for (auto i{1u}; i <= 11; i++)
		textManipulator.moveCursorPos(widget::LeftPos{});

	ASSERT_EQ(textManipulator.getCursorPos(), widget::Point(2_y, 0_x));

	for (auto i{1u}; i <= 11; i++)
		textManipulator.removeCh(widget::ForwardsRemove{});

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplus-ncursesplus\n");
}

TEST_F(ScrollableTextBoxWidgetTestFixture, text_box_manipulator_with_char_replacement_displays_text_replacement)
{
	std::string ncursesPlusLongText{"ncursesplus\nncursesplus-ncursesplus\nncursesplus"};

	widget::TextBoxProperties textBoxProperties;
	textBoxProperties.setReplacementChar('*');

	widget::TextManipulator<widget::ScrollMechanism::WITH_SCROLL> textManipulator{widget::Size{3_h, 18_w},
																											std::move(textBoxProperties)};
	textManipulator.setCursorPos(widget::Point{0_y, 0_x});

	for (auto ch: ncursesPlusLongText)
	{
		textManipulator.addCh(ch);
		textManipulator.moveCursorPos(widget::RightPos{});
	}

	ASSERT_EQ(textManipulator.getDisplayedText(), "***********************************");
}
