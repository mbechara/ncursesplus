#include "TestHelpers.hpp"
#include "LayoutStrategy.hpp"
#include "Point.hpp"
#include "Size.hpp"

namespace ncursesplus::test::helpers
{
DummyWidget::DummyWidget(const std::string& name, ncursesplus::widget::Size size, ncursesplus::widget::Point point)
	 : widget::Widget{name, size, point, widget::customization::WidgetColor{}}
{}

widget::Point DummyWidget::getCursor() const
{
	using namespace widget::literals;
	return widget::Point{0_y, 0_x};
}

void DummyWidget::handleKeyEventImpl(event::KeyEvent)
{}

void DummyWidget::moveCursorImpl() const
{}

void DummyWidget::drawImpl() const
{}

DummyWidgetBuilder::DummyWidgetBuilder(std::string name)
	 : WidgetBuilder{name}
{}

std::unique_ptr<widget::Widget> DummyWidgetBuilder::buildImpl()
{
	auto dummyWidget{std::make_unique<DummyWidget>(m_name, m_size, m_point)};
	return dummyWidget;
}
}
