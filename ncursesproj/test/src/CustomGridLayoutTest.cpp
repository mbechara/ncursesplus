#include "CustomGridLayoutStrategy.hpp"
#include "TestHelpers.hpp"
#include "WidgetCompositeBuilder.hpp"

#include <gtest/gtest.h>

using namespace ncursesplus;
using namespace widget::literals;
using namespace std::string_literals;

namespace
{
auto constructCustomGridLayoutStrategy()
{
	layout::CustomGridLayoutStrategy gridLayoutStrategy{layout::Rows<10u>{}, layout::Cols<10u>{}};
	gridLayoutStrategy.setParentSize(widget::Size{100_h, 100_w});
	gridLayoutStrategy.setParentPoint(widget::Point{0_y, 0_x});
	return gridLayoutStrategy;
}
}

TEST(CustomGridLayoutTest, custom_grid_layout_should_calculate_constraint_spanning_from_row_0_to_5_and_col_0_to_5)
{
	auto customGridLayoutStrategy{constructCustomGridLayoutStrategy()};

	auto widgetLayouts{customGridLayoutStrategy.layoutWidgets({layout::CustomGridLayoutConstraint{0, 5, 0, 5}})};

	const auto& [size, point]{widgetLayouts.front()};

	EXPECT_EQ(size, (widget::Size{50_h, 50_w}));
	EXPECT_EQ(point, (widget::Point{0_y, 0_x}));
}

TEST(CustomGridLayoutTest, custom_grid_layout_should_calculate_constraint_spanning_from_row_0_to_3_and_col_0_to_7)
{
	auto customGridLayoutStrategy{constructCustomGridLayoutStrategy()};

	auto widgetLayouts{customGridLayoutStrategy.layoutWidgets({layout::CustomGridLayoutConstraint{0, 3, 0, 7}})};

	const auto& [size, point]{widgetLayouts.front()};

	EXPECT_EQ(size, (widget::Size{30_h, 70_w}));
	EXPECT_EQ(point, (widget::Point{0_y, 0_x}));
}

TEST(CustomGridLayoutTest, custom_grid_layout_should_calculate_constraint_spanning_from_row_1_to_6_and_col_3_to_7)
{
	auto customGridLayoutStrategy{constructCustomGridLayoutStrategy()};

	auto widgetLayouts{customGridLayoutStrategy.layoutWidgets({layout::CustomGridLayoutConstraint{1, 6, 3, 7}})};

	const auto& [size, point]{widgetLayouts.front()};

	EXPECT_EQ(size, (widget::Size{50_h, 40_w}));
	EXPECT_EQ(point, (widget::Point{10_y, 30_x}));
}

TEST(
	CustomGridLayoutTest,
	widget_composite_builder_with_custom_grid_layout_should_calculate_widget1_spanning_from_row_1_to_5_and_col_3_to_7_and_widget2_spanning_from_row_5_to_8_and_col_7_to_10)
{
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{
			"WidgetComposite", widget::builder::NumOfWidgets<2>{},
			layout::CustomGridLayoutStrategy{layout::Rows<10u>{}, layout::Cols<10u>{}}}
			.withSize(widget::Size{100_h, 100_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}, layout::CustomGridLayoutConstraint{1, 6, 3, 7})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}, layout::CustomGridLayoutConstraint{5, 8, 7, 10})
			.build()};

	const auto* dummyWidget1 = find(widgetComposite.get(), "DummyWidget1");
	const auto* dummyWidget2 = find(widgetComposite.get(), "DummyWidget2");

	EXPECT_EQ(dummyWidget1->getSize(), (widget::Size{50_h, 40_w}));
	EXPECT_EQ(dummyWidget1->getPoint(), (widget::Point{10_y, 30_x}));

	EXPECT_EQ(dummyWidget2->getSize(), (widget::Size{30_h, 30_w}));
	EXPECT_EQ(dummyWidget2->getPoint(), (widget::Point{50_y, 70_x}));
}

TEST(CustomGridLayoutDeathTest, custom_grid_layout_should_abort_program_when_adding_constraint_intersecting_another)
{
	layout::CustomGridLayoutStrategy strategy{layout::Rows<10u>{}, layout::Cols<10u>{}};

	layout::CustomGridLayoutConstraint layoutConstraints[]{layout::CustomGridLayoutConstraint{0, 5, 0, 5},
																			 layout::CustomGridLayoutConstraint{2, 6, 2, 6}};

	EXPECT_DEATH(strategy.layoutWidgets(layoutConstraints), ".*");
}

TEST(
	CustomGridLayoutDeathTest,
	custom_grid_layout_should_abort_program_when_adding_constraint_with_rowEnd_and_colEnd_less_than_rowStart_and_colStart)
{
	layout::CustomGridLayoutStrategy strategy{layout::Rows<10u>{}, layout::Cols<10u>{}};

	EXPECT_DEATH(strategy.layoutWidgets({layout::CustomGridLayoutConstraint{5, 2, 0, 5}}), ".*");
	EXPECT_DEATH(strategy.layoutWidgets({layout::CustomGridLayoutConstraint{0, 5, 5, 2}}), ".*");
}

TEST(
	CustomGridLayoutDeathTest,
	custom_grid_layout_should_abort_program_when_adding_constraint_with_rowEnd_and_colEnd_exceeding_numOfRows_and_numOfCols)
{
	layout::CustomGridLayoutStrategy strategy{layout::Rows<10u>{}, layout::Cols<10u>{}};

	EXPECT_DEATH(strategy.layoutWidgets({layout::CustomGridLayoutConstraint{0, 11, 0, 5}}), ".*");
	EXPECT_DEATH(strategy.layoutWidgets({layout::CustomGridLayoutConstraint{0, 5, 0, 11}}), ".*");
}
