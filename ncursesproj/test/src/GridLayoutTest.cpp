#include "GridLayoutStrategy.hpp"
#include "TestHelpers.hpp"
#include "WidgetCompositeBuilder.hpp"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace ncursesplus;
using namespace widget::literals;
using namespace std::string_literals;

namespace
{
template<unsigned int NumOfRows, unsigned int NumOfCols>
auto constructGridLayoutStrategy(layout::Rows<NumOfRows> rows, layout::Cols<NumOfCols> cols)
{
	layout::GridLayoutStrategy gridLayoutStrategy{rows, cols};
	gridLayoutStrategy.setParentSize(widget::Size{100_h, 100_w});
	gridLayoutStrategy.setParentPoint(widget::Point{0_y, 0_x});
	return gridLayoutStrategy;
}
}

TEST(VerticalGridLayoutTest, grid_layout_1_row_1_col_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<1u>{}, layout::Cols<1u>{})};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets({layout::GridLayoutConstraint::VERTICAL})};

	const auto& [size, point]{widgetLayouts.front()};

	EXPECT_EQ(size, (widget::Size{100_h, 100_w}));
	EXPECT_EQ(point, (widget::Point{0_y, 0_x}));
}

TEST(VerticalGridLayoutTest, grid_layout_2_rows_2_cols_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<2u>{}, layout::Cols<2u>{})};

	layout::GridLayoutConstraint layoutConstraints[]{layout::GridLayoutConstraint::VERTICAL,
																	 layout::GridLayoutConstraint::VERTICAL};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets(layoutConstraints)};

	const auto& [size1, point1]{widgetLayouts[0]};
	const auto& [size2, point2]{widgetLayouts[1]};

	EXPECT_EQ(size1, (widget::Size{100_h, 50_w}));
	EXPECT_EQ(point1, (widget::Point{0_y, 0_x}));

	EXPECT_EQ(size2, (widget::Size{100_h, 50_w}));
	EXPECT_EQ(point2, (widget::Point{0_y, 50_x}));
}

TEST(VerticalGridLayoutTest, grid_layout_2_rows_3_cols_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<2u>{}, layout::Cols<3u>{})};

	layout::GridLayoutConstraint layoutConstraints[]{layout::GridLayoutConstraint::VERTICAL,
																	 layout::GridLayoutConstraint::VERTICAL,
																	 layout::GridLayoutConstraint::VERTICAL};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets(layoutConstraints)};

	const auto& [size1, point1]{widgetLayouts[0]};
	const auto& [size2, point2]{widgetLayouts[1]};
	const auto& [size3, point3]{widgetLayouts[2]};

	EXPECT_EQ(size1, (widget::Size{100_h, 33_w}));
	EXPECT_EQ(point1, (widget::Point{0_y, 0_x}));

	EXPECT_EQ(size2, (widget::Size{100_h, 33_w}));
	EXPECT_EQ(point2, (widget::Point{0_y, 33_x}));

	EXPECT_EQ(size3, (widget::Size{100_h, 33_w}));
	EXPECT_EQ(point3, (widget::Point{0_y, 66_x}));
}

TEST(HorizontalGridLayoutTest, grid_layout_1_row_1_col_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<1u>{}, layout::Cols<1u>{})};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets({layout::GridLayoutConstraint::HORIZONTAL})};

	const auto& [size, point]{widgetLayouts.front()};

	EXPECT_EQ(size, (widget::Size{100_h, 100_w}));
	EXPECT_EQ(point, (widget::Point{0_y, 0_x}));
}

TEST(HorizontalGridLayoutTest, grid_layout_2_rows_2_cols_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<2u>{}, layout::Cols<2u>{})};

	layout::GridLayoutConstraint layoutConstraints[]{layout::GridLayoutConstraint::HORIZONTAL,
																	 layout::GridLayoutConstraint::HORIZONTAL};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets(layoutConstraints)};

	const auto& [size1, point1]{widgetLayouts[0]};
	const auto& [size2, point2]{widgetLayouts[1]};

	EXPECT_EQ(size1, (widget::Size{50_h, 100_w}));
	EXPECT_EQ(point1, (widget::Point{0_y, 0_x}));

	EXPECT_EQ(size2, (widget::Size{50_h, 100_w}));
	EXPECT_EQ(point2, (widget::Point{50_y, 0_x}));
}

TEST(HorizontalGridLayoutTest, grid_layout_3_rows_2_cols_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<3u>{}, layout::Cols<2u>{})};

	layout::GridLayoutConstraint layoutConstraints[]{layout::GridLayoutConstraint::HORIZONTAL,
																	 layout::GridLayoutConstraint::HORIZONTAL,
																	 layout::GridLayoutConstraint::HORIZONTAL};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets(layoutConstraints)};

	const auto& [size1, point1]{widgetLayouts[0]};
	const auto& [size2, point2]{widgetLayouts[1]};
	const auto& [size3, point3]{widgetLayouts[2]};

	EXPECT_EQ(size1, (widget::Size{33_h, 100_w}));
	EXPECT_EQ(point1, (widget::Point{0_y, 0_x}));

	EXPECT_EQ(size2, (widget::Size{33_h, 100_w}));
	EXPECT_EQ(point2, (widget::Point{33_y, 0_x}));

	EXPECT_EQ(size3, (widget::Size{33_h, 100_w}));
	EXPECT_EQ(point3, (widget::Point{66_y, 0_x}));
}

TEST(NoConstraintGridLayoutTest, grid_layout_1_row_1_col_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<1u>{}, layout::Cols<1u>{})};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets({layout::GridLayoutConstraint::NO_CONSTRAINT})};

	const auto& [size, point]{widgetLayouts.front()};

	EXPECT_EQ(size, (widget::Size{100_h, 100_w}));
	EXPECT_EQ(point, (widget::Point{0_y, 0_x}));
}

TEST(NoConstraintGridLayoutTest, grid_layout_2_row_2_col_should_calculate_size_and_point)
{
	auto gridLayoutStrategy{constructGridLayoutStrategy(layout::Rows<2u>{}, layout::Cols<2u>{})};

	layout::GridLayoutConstraint layoutConstraints[]{
		layout::GridLayoutConstraint::NO_CONSTRAINT, layout::GridLayoutConstraint::NO_CONSTRAINT,
		layout::GridLayoutConstraint::NO_CONSTRAINT, layout::GridLayoutConstraint::NO_CONSTRAINT};

	auto widgetLayouts{gridLayoutStrategy.layoutWidgets(layoutConstraints)};

	const auto& [size1, point1]{widgetLayouts[0]};
	const auto& [size2, point2]{widgetLayouts[1]};
	const auto& [size3, point3]{widgetLayouts[2]};
	const auto& [size4, point4]{widgetLayouts[3]};

	EXPECT_EQ(size1, (widget::Size{50_h, 50_w}));
	EXPECT_EQ(point1, (widget::Point{0_y, 0_x}));

	EXPECT_EQ(size2, (widget::Size{50_h, 50_w}));
	EXPECT_EQ(point2, (widget::Point{0_y, 50_x}));

	EXPECT_EQ(size3, (widget::Size{50_h, 50_w}));
	EXPECT_EQ(point3, (widget::Point{50_y, 0_x}));

	EXPECT_EQ(size4, (widget::Size{50_h, 50_w}));
	EXPECT_EQ(point4, (widget::Point{50_y, 50_x}));
}

TEST(
	GridLayoutTest,
	widget_composite_builder_with_grid_layout_2_rows_2_cols_should_calculate_vertical_and_horizontal_constraint_widget_size_and_point)
{
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<3>{},
															 layout::GridLayoutStrategy{layout::Rows<2u>{}, layout::Cols<2u>{}}}
			.withSize(widget::Size{100_h, 100_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}, layout::GridLayoutConstraint::VERTICAL)
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}, layout::GridLayoutConstraint::HORIZONTAL)
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget3"}, layout::GridLayoutConstraint::HORIZONTAL)
			.build()};

	const auto* dummyWidget1 = find(widgetComposite.get(), "DummyWidget1");
	const auto* dummyWidget2 = find(widgetComposite.get(), "DummyWidget2");
	const auto* dummyWidget3 = find(widgetComposite.get(), "DummyWidget3");

	EXPECT_EQ(dummyWidget1->getSize(), (widget::Size{100_h, 50_w}));
	EXPECT_EQ(dummyWidget1->getPoint(), (widget::Point{0_y, 0_x}));

	EXPECT_EQ(dummyWidget2->getSize(), (widget::Size{50_h, 50_w}));
	EXPECT_EQ(dummyWidget2->getPoint(), (widget::Point{0_y, 50_x}));

	EXPECT_EQ(dummyWidget3->getSize(), (widget::Size{50_h, 50_w}));
	EXPECT_EQ(dummyWidget3->getPoint(), (widget::Point{50_y, 50_x}));
}

TEST(GridLayoutDeathTest, grid_layout_should_abort_program_when_adding_more_subwidgets_than_the_maximum_it_can_fit)
{
	layout::GridLayoutStrategy strategy{layout::Rows<2u>{}, layout::Cols<2u>{}};

	layout::GridLayoutConstraint layoutConstraints[]{
		layout::GridLayoutConstraint::NO_CONSTRAINT, layout::GridLayoutConstraint::NO_CONSTRAINT,
		layout::GridLayoutConstraint::NO_CONSTRAINT, layout::GridLayoutConstraint::NO_CONSTRAINT,
		layout::GridLayoutConstraint::NO_CONSTRAINT};

	EXPECT_DEATH(strategy.layoutWidgets(layoutConstraints), ".*");
}

TEST(GridLayoutDeathTest,
	  grid_layout_3_rows_3_cols_should_abort_program_if_a_widget_was_added_without_remaining_slots_in_the_grid_part1)
{
	layout::GridLayoutStrategy strategy{layout::Rows<3u>{}, layout::Cols<3u>{}};

	layout::GridLayoutConstraint layoutConstraints[]{
		layout::GridLayoutConstraint::HORIZONTAL, layout::GridLayoutConstraint::VERTICAL,
		layout::GridLayoutConstraint::HORIZONTAL, layout::GridLayoutConstraint::HORIZONTAL,
		layout::GridLayoutConstraint::NO_CONSTRAINT};

	EXPECT_DEATH(strategy.layoutWidgets(layoutConstraints), ".*");
}

TEST(GridLayoutDeathTest,
	  grid_layout_3_rows_3_cols_should_abort_program_if_a_widget_was_added_without_remaining_slots_in_the_grid_part2)
{
	layout::GridLayoutStrategy strategy{layout::Rows<3u>{}, layout::Cols<3u>{}};

	layout::GridLayoutConstraint layoutConstraints[]{
		layout::GridLayoutConstraint::NO_CONSTRAINT, layout::GridLayoutConstraint::VERTICAL,
		layout::GridLayoutConstraint::HORIZONTAL,    layout::GridLayoutConstraint::VERTICAL,
		layout::GridLayoutConstraint::NO_CONSTRAINT, layout::GridLayoutConstraint::HORIZONTAL,
		layout::GridLayoutConstraint::NO_CONSTRAINT};

	EXPECT_DEATH(strategy.layoutWidgets(layoutConstraints), ".*");
}

TEST(GridLayoutDeathTest,
	  grid_layout_3_rows_3_cols_should_abort_program_if_a_widget_was_added_without_remaining_slots_in_the_grid_part3)
{
	layout::GridLayoutStrategy strategy{layout::Rows<3u>{}, layout::Cols<3u>{}};

	layout::GridLayoutConstraint layoutConstraints[]{
		layout::GridLayoutConstraint::VERTICAL, layout::GridLayoutConstraint::HORIZONTAL,
		layout::GridLayoutConstraint::VERTICAL, layout::GridLayoutConstraint::VERTICAL,
		layout::GridLayoutConstraint::NO_CONSTRAINT};

	EXPECT_DEATH(strategy.layoutWidgets(layoutConstraints), ".*");
}
