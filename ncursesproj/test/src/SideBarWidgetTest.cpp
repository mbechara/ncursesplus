#include "SideBarWidget.hpp"
#include "NcursesPlusTerminalTestFixture.hpp"

using namespace ncursesplus;
using namespace widget::literals;
using namespace std::string_literals;

class SideBarWidget3x10TestFixture : public test::NcursesPlusTerminalTestFixture
{
protected:
	SideBarWidget3x10TestFixture()
		 : test::NcursesPlusTerminalTestFixture{50, 50}
	{
		m_sideBarWidget.moveCursor();
		m_sideBarWidget.addMenuEntries("Menu1"s, "Menu2"s, "Menu3"s);
	}
	widget::SideBarWidget<3u, 10u> m_sideBarWidget{"SideBarWidget", widget::Size{5_h, 5_w}, widget::Point{0_y, 0_x},
																  widget::customization::WidgetColor{}};
};

TEST_F(SideBarWidget3x10TestFixture, side_bar_has_first_menu_entry_selected_by_default)
{
	EXPECT_EQ(m_sideBarWidget.getText(), "Menu1");
}

TEST_F(SideBarWidget3x10TestFixture, down_arrow_on_sidebar_selects_second_menu_entry)
{
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	EXPECT_EQ(m_sideBarWidget.getText(), "Menu2");
}

TEST_F(SideBarWidget3x10TestFixture, two_down_arrows_on_sidebar_selects_third_menu_entry)
{
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	EXPECT_EQ(m_sideBarWidget.getText(), "Menu3");
}

TEST_F(SideBarWidget3x10TestFixture, down_arrows_surpassing_menu_entry_count_on_sidebar_keeps_last_menu_entry_selected)
{
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	EXPECT_EQ(m_sideBarWidget.getText(), "Menu3");
}

TEST_F(SideBarWidget3x10TestFixture, up_arrow_from_last_entry_on_sidebar_selects_second_menu_entry)
{
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	EXPECT_EQ(m_sideBarWidget.getText(), "Menu2");
}

TEST_F(SideBarWidget3x10TestFixture, up_arrow_from_second_entry_on_sidebar_selects_first_menu_entry)
{
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	EXPECT_EQ(m_sideBarWidget.getText(), "Menu1");
}

TEST_F(SideBarWidget3x10TestFixture, up_arrows_surpassing_menu_entry_count_on_sidebar_keeps_first_menu_entry_selected)
{
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_sideBarWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	EXPECT_EQ(m_sideBarWidget.getText(), "Menu1");
}

class SideBarWidget3x20TestFixture : public test::NcursesPlusTerminalTestFixture
{
protected:
	SideBarWidget3x20TestFixture()
		 : test::NcursesPlusTerminalTestFixture{50, 50}
	{
		m_sideBarWidget.moveCursor();
		m_sideBarWidget.addMenuEntries("Menu1"s, "Menu2"s, "Menu3"s);
	}
	widget::SideBarWidget<3u, 20u> m_sideBarWidget{"SideBarWidget", widget::Size{50_h, 10_w}, widget::Point{0_y, 0_x},
																widget::customization::WidgetColor{}};
};

TEST_F(SideBarWidget3x20TestFixture, side_bar_of_50_h_10_w_has_menu_entry_size_10_h_10_w)
{
	ASSERT_EQ(m_sideBarWidget.getMenuEntrySize(), widget::Size(10_h, 10_w));
}

TEST_F(SideBarWidget3x20TestFixture,
		 side_bar_of_50_h_10_w_has_menu_entry_entry_points_10_h_apart)
{
	ASSERT_EQ(m_sideBarWidget.getMenuEntryPoint("Menu1"), widget::Point(0_y, 0_x));
	ASSERT_EQ(m_sideBarWidget.getMenuEntryPoint("Menu2"), widget::Point(9_y, 0_x));
	ASSERT_EQ(m_sideBarWidget.getMenuEntryPoint("Menu3"), widget::Point(18_y, 0_x));
}
