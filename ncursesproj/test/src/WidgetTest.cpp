#include "TestHelpers.hpp"
#include "WidgetColor.hpp"
#include "WidgetCompositeBuilder.hpp"
#include <ranges>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace ncursesplus;

class WidgetTestFixture : public ::testing::Test
{
public:
	WidgetTestFixture()
		 : m_widgetComposite{"WidgetComposite1", widget::Size{}, widget::Point{}, widget::customization::WidgetColor{}}
	{
		m_widgetComposite.add(
			std::make_unique<test::helpers::DummyWidget>("DummyWidget1", widget::Size{}, widget::Point{}));
		auto widgetComposite2{std::make_unique<widget::WidgetComposite>(
			"WidgetComposite2", widget::Size{}, widget::Point{}, widget::customization::WidgetColor{})};
		widgetComposite2->add(
			std::make_unique<test::helpers::DummyWidget>("DummyWidget2", widget::Size{}, widget::Point{}));
		m_widgetComposite.add(std::move(widgetComposite2));
		m_widgetComposite.add(
			std::make_unique<test::helpers::DummyWidget>("DummyWidget3", widget::Size{}, widget::Point{}));
	}
protected:
	widget::WidgetComposite m_widgetComposite;
};

TEST_F(WidgetTestFixture, parent_widget_should_contain_direct_subwidget)
{
	EXPECT_NE(find(&m_widgetComposite, "DummyWidget1"), nullptr);
}

TEST_F(WidgetTestFixture, parent_widget_should_contain_transitive_subwidget)
{
	EXPECT_NE(find(&m_widgetComposite, "DummyWidget2"), nullptr);
}

TEST_F(WidgetTestFixture, parent_should_traverse_composite_hierarchy)
{
	std::vector<std::string> reachedWidgets;

	m_widgetComposite.traverse([&reachedWidgets](const auto* widget) {
		reachedWidgets.push_back(std::string{widget->getName()});
	});

	EXPECT_THAT(reachedWidgets, ::testing::ElementsAre("WidgetComposite1", "DummyWidget1", "WidgetComposite2",
																		"DummyWidget2", "DummyWidget3"));
}

using namespace widget::literals;

TEST(WidgetBuilderTest, widget_builder_asActive_should_declare_widget_as_active)
{
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<2>{},
															 test::helpers::DummyLayoutStrategy{}}
			.withSize(widget::Size{100_h, 50_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}, test::helpers::DummyLayoutConstraint{})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}.asActive(), test::helpers::DummyLayoutConstraint{})
			.build()};

	const auto* widget1 = find(widgetComposite.get(), "DummyWidget1");
	const auto* widget2 = find(widgetComposite.get(), "DummyWidget2");

	EXPECT_FALSE(widget1->isActiveWidget());
	EXPECT_TRUE(widget2->isActiveWidget());
}

TEST(WidgetBuilderDeathTest, widget_builder_should_abort_program_if_more_than_one_widget_is_declared_as_active)
{
	auto builder{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<2>{},
															 test::helpers::DummyLayoutStrategy{}}
			.withSize(widget::Size{100_h, 50_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}.asActive(), test::helpers::DummyLayoutConstraint{})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}.asActive(), test::helpers::DummyLayoutConstraint{})};

	EXPECT_DEATH(builder.build(), ".*");
}

TEST(WidgetColorTest, color_combinations_size_test)
{
	constexpr auto colorCombinations{widget::customization::generateColorCombinations()};
	EXPECT_THAT(colorCombinations, testing::SizeIs(81));
}

TEST(WidgetColorTest, color_combinations_default_color_recurrence_test)
{
	constexpr auto colorCombinations{widget::customization::generateColorCombinations()};
	auto foregroundColors{colorCombinations | std::views::keys};
	auto defaultColorCount{std::ranges::count_if(foregroundColors, [](auto foregroundColor) {
		return foregroundColor == widget::customization::Color::DEFAULT;
	})};
	EXPECT_EQ(defaultColorCount, 9);
}
