#include "KeyEventObserver.hpp"
#include "NcursesPlusTerminalTestFixture.hpp"
#include "NeighborWidgetsMediator.hpp"
#include "TestHelpers.hpp"
#include "WidgetCompositeBuilder.hpp"

#include <gmock/gmock.h>
#include <memory>
#include <stdexcept>
#include <vector>

using namespace ncursesplus;
using namespace widget::literals;

MATCHER(IsKeyCombinations, "")
{
	return arg.isKeyCombination();
}

TEST(KeyEventTest, ctrl_keys_are_key_combinations_test)
{
	std::vector<event::KeyEvent> ctrlKeys{0x08, 0x0a, 0x0b, 0x0c};
	EXPECT_THAT(ctrlKeys, ::testing::Each(IsKeyCombinations()));
}

class MockWidget final : public widget::Widget
{
public:
	MockWidget(const std::string& name)
		 : widget::Widget{name, widget::Size{}, widget::Point{}, widget::customization::WidgetColor{}}
	{}

	MOCK_METHOD(widget::Point, getCursor, (), (const, override));
	MOCK_METHOD(void, handleKeyEventImpl, (event::KeyEvent), (override));
	MOCK_METHOD(void, moveCursorImpl, (), (const, override));
	MOCK_METHOD(void, drawImpl, (), (const, override));
};

class KeyEventHandlerTest : public test::NcursesPlusTerminalTestFixture
{
protected:
	KeyEventHandlerTest()
		 : test::NcursesPlusTerminalTestFixture{100, 100}
	{}
};

TEST_F(KeyEventHandlerTest, active_widget_should_handle_event)
{
	auto mockWidget1{std::make_unique<MockWidget>("MockWidget1")};
	auto mockWidget2{std::make_unique<MockWidget>("MockWidget2")};
	mockWidget2->moveCursor();

	EXPECT_CALL(*mockWidget1, handleKeyEventImpl(::testing::_)).Times(0);
	EXPECT_CALL(*mockWidget2, handleKeyEventImpl(::testing::_)).Times(1);

	widget::WidgetComposite widgetComposite{"WidgetComposite", widget::Size{}, widget::Point{},
														 widget::customization::WidgetColor{}};

	widgetComposite.add(std::move(mockWidget1));
	widgetComposite.add(std::move(mockWidget2));

	widgetComposite.handleKeyEvent(event::KeyEvent{0x20});
}

TEST_F(KeyEventHandlerTest, widget_within_nested_widget_composite_should_handle_event)
{
	auto mockWidget{std::make_unique<MockWidget>("MockWidget")};

	EXPECT_CALL(*mockWidget, handleKeyEventImpl(::testing::_)).Times(0);

	auto widgetComposite2{std::make_unique<widget::WidgetComposite>("WidgetComposite2", widget::Size{}, widget::Point{},
																						 widget::customization::WidgetColor{})};
	widgetComposite2->add(std::move(mockWidget));

	widget::WidgetComposite widgetComposite1{"WidgetComposite1", widget::Size{}, widget::Point{},
														  widget::customization::WidgetColor{}};
	widgetComposite1.add(std::move(widgetComposite2));

	widgetComposite1.handleKeyEvent(event::KeyEvent{0x20});
}

TEST_F(KeyEventHandlerTest, should_move_cursor_to_top_widget_when_sending_ctrl_k_event)
{
	auto mediator{std::make_shared<widget::NeighborWidgetsMediator>()};

	auto widgetComposite{widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<2>{},
																					 test::helpers::StubLayoutStrategy{}}
									.withSize(widget::Size{100_h, 50_w})
									.withPoint(widget::Point{0_y, 0_x})
									.add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}.withMediator(mediator),
										  test::helpers::StubLayoutConstraint{widget::Size{50_h, 50_w}, widget::Point{0_y, 0_x}})
									.add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}.withMediator(mediator).asActive(),
										  test::helpers::StubLayoutConstraint{widget::Size{50_h, 50_w}, widget::Point{50_y, 0_x}})
									.build()};

	const auto* dummyWidget1 = find(widgetComposite.get(), "DummyWidget1");
	const auto* dummyWidget2 = find(widgetComposite.get(), "DummyWidget2");

	EXPECT_TRUE(dummyWidget2->isActiveWidget());
	EXPECT_FALSE(dummyWidget1->isActiveWidget());

	widgetComposite->handleKeyEvent(event::KeyEvent(0x0b));

	EXPECT_TRUE(dummyWidget1->isActiveWidget());
	EXPECT_FALSE(dummyWidget2->isActiveWidget());
}

TEST_F(KeyEventHandlerTest, widget_without_mediator_should_not_handle_event_and_attempt_to_move_cursor)
{
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<2>{},
															 test::helpers::DummyLayoutStrategy{}}
			.withSize(widget::Size{100_h, 50_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}, test::helpers::DummyLayoutConstraint{})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}.asActive(), test::helpers::DummyLayoutConstraint{})
			.build()};

	const auto* dummyWidget1 = find(widgetComposite.get(), "DummyWidget1");
	const auto* dummyWidget2 = find(widgetComposite.get(), "DummyWidget2");

	EXPECT_TRUE(dummyWidget2->isActiveWidget());
	EXPECT_FALSE(dummyWidget1->isActiveWidget());

	widgetComposite->handleKeyEvent(event::KeyEvent(0x0b));

	EXPECT_TRUE(dummyWidget2->isActiveWidget());
	EXPECT_FALSE(dummyWidget1->isActiveWidget());
}

TEST(ReadingInputFromWidgetTest, should_throw_if_trying_to_read_from_non_active_widget)
{
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<1>{},
															 test::helpers::DummyLayoutStrategy{}}
			.withSize(widget::Size{100_h, 50_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget"}, test::helpers::DummyLayoutConstraint{})
			.build()};

	auto dummyWidget{test::helpers::DummyWidget{"DummyWidget", widget::Size{0_h, 0_w}, widget::Point{0_y, 0_x}}};

	EXPECT_THROW(dummyWidget.read(), std::logic_error);
}

TEST(ReadingInputFromWidgetTest, should_read_input_from_active_widget)
{
	auto widgetComposite{widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<1>{},
																					 test::helpers::StubLayoutStrategy{}}
									.withSize(widget::Size{100_h, 50_w})
									.withPoint(widget::Point{0_y, 0_x})
									.add(test::helpers::DummyWidgetBuilder{"DummyWidget"}.asActive(),
										  test::helpers::StubLayoutConstraint{widget::Size{50_h, 50_w}, widget::Point{0_y, 0_x}})
									.build()};

	ungetch(0x20);
	auto ch{widget::readInput(widgetComposite.get())};
	EXPECT_EQ(ch, 0x20);
}
