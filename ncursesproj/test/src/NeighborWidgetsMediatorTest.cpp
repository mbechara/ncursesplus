#include "NeighborWidgetsMediator.hpp"
#include "NcursesPlusTerminalTestFixture.hpp"
#include "TestHelpers.hpp"
#include "WidgetCompositeBuilder.hpp"

#include <gtest/gtest.h>

using namespace ncursesplus;
using namespace widget::literals;

class NeighborWidgetsMediatorTestFixture : public test::NcursesPlusTerminalTestFixture
{
protected:
	NeighborWidgetsMediatorTestFixture()
		 : test::NcursesPlusTerminalTestFixture{250, 250}
	{}
protected:
	std::shared_ptr<widget::NeighborWidgetsMediator> m_mediator{new widget::NeighborWidgetsMediator{}};
};

TEST_F(NeighborWidgetsMediatorTestFixture, mediator_should_not_find_any_widget_to_move_cursor_to)
{
	test::helpers::DummyWidget dummyWidget{"DummyWidget", widget::Size{50_h, 50_w}, widget::Point{0_y, 0_x}};
	dummyWidget.moveCursor();

	m_mediator->add(&dummyWidget);

	m_mediator->notify(&dummyWidget, "TOP");
	m_mediator->notify(&dummyWidget, "RIGHT");
	m_mediator->notify(&dummyWidget, "BOTTOM");
	m_mediator->notify(&dummyWidget, "LEFT");

	EXPECT_TRUE(dummyWidget.isActiveWidget());
}

class NeighborWidgetsParametrizedTestFixture
	 : public ::testing::WithParamInterface<std::pair<std::string, widget::Point>>,
		public NeighborWidgetsMediatorTestFixture
{};

INSTANTIATE_TEST_SUITE_P(NeighborWidgetsMediatorTest, NeighborWidgetsParametrizedTestFixture,
								 ::testing::Values(std::make_pair("TOP", widget::Point{0_y, 50_x}),
														 std::make_pair("RIGHT", widget::Point{50_y, 100_x}),
														 std::make_pair("BOTTOM", widget::Point{100_y, 50_x}),
														 std::make_pair("LEFT", widget::Point{50_y, 0_x})),
								 [](const auto& sizeAndDestinationPoint) {
									 auto side = std::get<0>(sizeAndDestinationPoint.param);
									 auto destinationPoint = std::get<1>(sizeAndDestinationPoint.param);
									 return std::string{side} + "_requested_side_and_destinaion_at_point_" +
											  std::to_string(destinationPoint.getY()) + "_y_" +
											  std::to_string(destinationPoint.getX()) + "_x";
								 });

TEST_P(NeighborWidgetsParametrizedTestFixture, mediator_should_find_widget_at_requested_side)
{
	auto [side, destinationPoint] = GetParam();

	test::helpers::DummyWidget dummyWidget1{"DummyWidget1", widget::Size{50_h, 50_w}, widget::Point{50_y, 50_x}};
	dummyWidget1.moveCursor();

	test::helpers::DummyWidget dummyWidget2{"DummyWidget2", widget::Size{50_h, 50_w}, destinationPoint};

	m_mediator->add(&dummyWidget1);
	m_mediator->add(&dummyWidget2);

	EXPECT_TRUE(dummyWidget1.isActiveWidget());

	m_mediator->notify(&dummyWidget1, side);

	EXPECT_TRUE(dummyWidget2.isActiveWidget());
	EXPECT_FALSE(dummyWidget1.isActiveWidget());
}

class ClosestNeighborWidgetsParametrizedTestFixture
	 : public ::testing::WithParamInterface<std::tuple<std::string, widget::Point, widget::Point>>,
		public NeighborWidgetsMediatorTestFixture
{};

INSTANTIATE_TEST_SUITE_P(
	NeighborWidgetsMediatorTest, ClosestNeighborWidgetsParametrizedTestFixture,
	::testing::Values(std::make_tuple("TOP", widget::Point{50_y, 100_x}, widget::Point{0_y, 100_x}),
							std::make_tuple("RIGHT", widget::Point{100_y, 150_x}, widget::Point{100_y, 200_x}),
							std::make_tuple("BOTTOM", widget::Point{150_y, 100_x}, widget::Point{200_y, 100_x}),
							std::make_tuple("LEFT", widget::Point{100_y, 50_x}, widget::Point{100_y, 0_x})),
	[](const auto& sizeAndDestinationPoint) {
		auto side = std::get<0>(sizeAndDestinationPoint.param);
		auto destinationPoint = std::get<1>(sizeAndDestinationPoint.param);
		return std::string{side} + "_requested_side_and_closest_widget_at_point_" +
				 std::to_string(destinationPoint.getY()) + "_y_" + std::to_string(destinationPoint.getX()) + "_x";
	});

TEST_P(ClosestNeighborWidgetsParametrizedTestFixture, mediator_should_find_closest_widget_at_requested_side)
{
	auto [side, closestPoint, farthestPoint] = GetParam();

	test::helpers::DummyWidget dummyWidget1{"DummyWidget1", widget::Size{50_h, 50_w}, widget::Point{100_y, 100_x}};
	dummyWidget1.moveCursor();

	test::helpers::DummyWidget dummyWidget2{"DummyWidget2", widget::Size{50_h, 50_w}, closestPoint};
	test::helpers::DummyWidget dummyWidget3{"DummyWidget3", widget::Size{50_h, 50_w}, farthestPoint};

	m_mediator->add(&dummyWidget1);
	m_mediator->add(&dummyWidget2);
	m_mediator->add(&dummyWidget3);

	EXPECT_TRUE(dummyWidget1.isActiveWidget());

	m_mediator->notify(&dummyWidget1, side);

	EXPECT_TRUE(dummyWidget2.isActiveWidget());
	EXPECT_FALSE(dummyWidget1.isActiveWidget());
	EXPECT_FALSE(dummyWidget3.isActiveWidget());
}

TEST_F(NeighborWidgetsMediatorTestFixture,
		 widgets_with_mediator_should_not_find_other_widgets_on_different_sides_than_requested_one)
{
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<4>{},
															 test::helpers::StubLayoutStrategy{}}
			.withSize(widget::Size{250_h, 250_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}.withMediator(m_mediator).asActive(),
				  test::helpers::StubLayoutConstraint{widget::Size{50_h, 50_w}, widget::Point{50_y, 50_x}})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}.withMediator(m_mediator),
				  test::helpers::StubLayoutConstraint{widget::Size{50_h, 50_w}, widget::Point{50_y, 100_x}})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget3"}.withMediator(m_mediator),
				  test::helpers::StubLayoutConstraint{widget::Size{50_h, 50_w}, widget::Point{100_y, 50_x}})
			.add(test::helpers::DummyWidgetBuilder{"DummyWidget4"}.withMediator(m_mediator),
				  test::helpers::StubLayoutConstraint{widget::Size{50_h, 50_w}, widget::Point{50_y, 0_x}})
			.build()};

	const auto* dummyWidget1{static_cast<const widget::Widget*>(find(widgetComposite.get(), "DummyWidget1"))};

	const auto* dummyWidget2{find(widgetComposite.get(), "DummyWidget2")};
	const auto* dummyWidget3{find(widgetComposite.get(), "DummyWidget3")};
	const auto* dummyWidget4{find(widgetComposite.get(), "DummyWidget4")};

	EXPECT_TRUE(dummyWidget1->isActiveWidget());

	m_mediator->notify(dummyWidget1, "TOP");

	EXPECT_TRUE(dummyWidget1->isActiveWidget());
	EXPECT_FALSE(dummyWidget2->isActiveWidget());
	EXPECT_FALSE(dummyWidget3->isActiveWidget());
	EXPECT_FALSE(dummyWidget4->isActiveWidget());
}

TEST_F(NeighborWidgetsMediatorTestFixture, widgets_with_mediator_should_find_subwidget_in_other_composite)
{
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite1", widget::builder::NumOfWidgets<2>{},
															 test::helpers::StubLayoutStrategy{}}
			.withSize(widget::Size{100_h, 100_w})
			.withPoint(widget::Point{0_y, 0_x})
			.add(widget::builder::WidgetCompositeBuilder{"WidgetComposite2", widget::builder::NumOfWidgets<1>{},
																		test::helpers::StubLayoutStrategy{}}
					  .add(test::helpers::DummyWidgetBuilder{"DummyWidget1"}.withMediator(m_mediator).asActive(),
							 test::helpers::StubLayoutConstraint{widget::Size{100_h, 50_w}, widget::Point{0_y, 0_x}}),
				  test::helpers::StubLayoutConstraint{widget::Size{100_h, 50_w}, widget::Point{0_y, 0_x}})
			.add(widget::builder::WidgetCompositeBuilder{"WidgetComposite3", widget::builder::NumOfWidgets<1>{},
																		test::helpers::StubLayoutStrategy{}}
					  .add(test::helpers::DummyWidgetBuilder{"DummyWidget2"}.withMediator(m_mediator),
							 test::helpers::StubLayoutConstraint{widget::Size{100_h, 50_w}, widget::Point{0_y, 50_x}}),
				  test::helpers::StubLayoutConstraint{widget::Size{100_h, 50_w}, widget::Point{0_y, 5_x}})
			.build()};

	auto* dummyWidget1 = static_cast<const widget::Widget*>(find(widgetComposite.get(), "DummyWidget1"));

	EXPECT_TRUE(dummyWidget1->isActiveWidget());

	m_mediator->notify(dummyWidget1, "RIGHT");

	auto* dummyWidget2 = find(widgetComposite.get(), "DummyWidget2");

	EXPECT_TRUE(dummyWidget2->isActiveWidget());
	EXPECT_FALSE(dummyWidget1->isActiveWidget());
}
