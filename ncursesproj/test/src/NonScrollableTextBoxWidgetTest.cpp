#include "NcursesPlusTerminalTestFixture.hpp"
#include "Size.hpp"
#include "TextBoxWidget.hpp"
#include <gtest/gtest.h>

using namespace ncursesplus;
using namespace widget::literals;

class NonScrollableTextBoxWidgetTestFixture : public test::NcursesPlusTerminalTestFixture
{
protected:
	NonScrollableTextBoxWidgetTestFixture()
		 : test::NcursesPlusTerminalTestFixture{50, 50},
			m_textBoxWidget{"TextBoxWidget", widget::Size{5_h, 20_w}, widget::Point{0_y, 0_x}, widget::TextBoxProperties{},
								 widget::customization::WidgetColor{}}
	{
		m_textBoxWidget.moveCursor();
	}
protected:
	widget::TextBoxWidget<widget::ScrollMechanism::WITHOUT_SCROLL> m_textBoxWidget;
};

constexpr std::string_view ncursesplusText{"ncursesplus"};
constexpr std::string_view ncursesplusLongText{"ncursesplus-ncursesplus"};

TEST_F(NonScrollableTextBoxWidgetTestFixture, text_box_widget_properly_return_text)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, text_box_widget_properly_return_text_spanning_on_two_lines)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusLongText);
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, backspace_removes_one_character_to_the_left)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 10_x}));
	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplu");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, backspace_on_empty_text_returns_empty_string)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});
	auto text = m_textBoxWidget.getText();
	ASSERT_EQ(text, "");
	ASSERT_EQ(text.size(), 0u);
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, backspace_several_times_on_empty_text_returns_empty_string)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});
	auto text = m_textBoxWidget.getText();
	ASSERT_EQ(text, "");
	ASSERT_EQ(text.size(), 0u);
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, left_arrow_moves_cursor_one_character_to_the_left)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 10_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, left_arrow_on_empty_text_keeps_cursor_on_same_position)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, right_arrow_moves_cursor_one_character_to_the_right)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 10_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, right_arrow_does_not_pass_text_limit)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, right_arrow_does_not_pass_text_limit_on_second_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, del_removes_one_character_to_the_right)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplu");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, del_on_empty_text_returns_empty_string)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	auto text = m_textBoxWidget.getText();
	ASSERT_EQ(text, "");
	ASSERT_EQ(text.size(), 0u);
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, del_several_times_on_empty_text_returns_empty_string)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	auto text = m_textBoxWidget.getText();
	ASSERT_EQ(text, "");
	ASSERT_EQ(text.size(), 0u);
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 del_removes_one_character_to_the_right_and_shifts_other_characters_on_same_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursespus");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_removes_one_character_to_the_left_and_shifts_other_characters_on_same_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncurseslus");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, up_arrow_moves_cursor_one_line_up)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, down_arrow_with_empty_next_line_does_not_move_cursor)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, down_arrow_moves_cursor_one_line_down)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 down_arrow_on_column_after_end_of_text_on_second_line_moves_cursor_to_last_character_on_second_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 7_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 down_arrow_on_column_before_end_of_text_on_second_line_moves_cursor_to_same_column_on_second_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 3_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 3_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 del_removes_one_character_to_the_right_and_pulls_text_spanning_second_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	ASSERT_EQ(m_textBoxWidget.getText(), "ncurssplus-ncursesplus");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 4_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_removes_one_character_to_the_left_and_pulls_text_spanning_second_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncuresplus-ncursesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 4_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 4_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, left_arrow_all_the_way_takes_cursor_up_one_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 17_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, enter_key_adds_a_newline_and_moves_cursor_to_start_of_newline)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus\nncursesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, up_arrow_after_enter_key_goes_up_one_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_in_middle_of_line_with_newline_character_at_the_end_should_not_pull_text_on_second_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});
	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplu\nncursesplus");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 10_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 del_in_middle_of_line_with_newline_character_at_the_end_should_not_pull_text_on_second_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplu\nncursesplus");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 10_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 left_arrow_all_the_way_on_second_line_after_a_newline_takes_cursor_to_the_end_of_previous_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 12; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, left_arrow_after_newline_character_takes_cursor_back_to_newline_character)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, del_newline_character_pulls_text_on_second_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 12; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplusncursesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, right_arrow_all_the_way_takes_cursor_down_one_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 5_x}));

	for (auto i{1u}; i <= 18; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(
	NonScrollableTextBoxWidgetTestFixture,
	right_arrow_all_the_way_on_first_line_with_newline_at_the_end_takes_cursor_to_the_continuation_of_text_on_second_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 right_arrow_all_the_way_with_newline_takes_cursor_to_start_of_second_line_even_if_its_empty)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus\n");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));

	for (auto i{1u}; i <= 12; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));
}

TEST_F(
	NonScrollableTextBoxWidgetTestFixture,
	right_arrow_all_the_way_on_text_fully_filling_first_line_should_take_cursor_to_first_column_of_empty_second_line_only)
{
	std::string ncursesplusText{"ncursesplus-ncurse"};
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	for (auto i{1u}; i <= 18; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_after_single_newline_on_previous_line_removes_newline_and_goes_to_start_of_previous_line)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));
	ASSERT_EQ(m_textBoxWidget.getText(), "\n");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));
	ASSERT_EQ(m_textBoxWidget.getText(), "");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_on_empty_line_after_a_newline_will_go_to_the_end_of_previous_line_and_remove_newline)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus\n");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusText);
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_on_first_column_of_second_line_removes_last_character_on_previous_line_and_shift_text)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 5; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncurssplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 17_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, backspace_on_second_column_second_line_does_not_shift_all_text)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{' '});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), " \n");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{' '});

	ASSERT_EQ(m_textBoxWidget.getText(), " \n ");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 1_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), " \n");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), " ");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 up_arrow_on_column_after_end_of_text_on_previous_line_moves_cursor_to_last_character_on_previous_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus\n\nncursesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 11_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 down_arrow_on_first_column_of_second_line_does_not_move_cursor_if_third_line_is_empty)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 11; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 down_arrow_does_not_pass_last_column_on_second_line_with_non_empty_third_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	for (auto i{1u}; i <= 16; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 17_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 del_should_not_shift_entire_text_and_should_keep_character_after_newline_intact)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	for (auto i{1u}; i <= 16; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 17_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 4_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 1_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 del_newline_does_not_cause_sentinel_character_to_change_position_and_mess_the_text_traversal)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 1_x}));

	for (auto i{1u}; i <= 4; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 6_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, backspace_after_several_newlines_removes_previous_newline)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), "\n\n");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), "\n");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, del_in_middle_with_newline_at_last_column_should_not_shift_other_lines)
{
	std::string ncursesplusText{"ncursesplus-ncurs"};
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getText(), "cursesplus-ncurs\n\n");

	for (auto i{1u}; i <= 16; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, empty_line_after_del_should_be_removed)
{
	std::string ncursesplusText{"ncursesplus-ncur"};
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'s'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'e'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	for (auto i{1u}; i <= 15; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncurs\ne");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 1_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, up_arrow_on_empty_text_keeps_cursor_at_same_position)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_several_times_to_completely_erase_text_empties_text_and_repositions_cursor_to_beginning)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'c'});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), "");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, text_after_newline_character_properly_adds_character_to_the_text)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), "\nn");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 1_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 del_newline_with_one_line_under_it_then_down_arrow_keeps_cursor_position_at_first_line)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));
	ASSERT_EQ(m_textBoxWidget.getText(), "n");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_character_at_start_of_text_shifts_existing_character_one_place_to_the_right)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'c'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), "nc");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 2_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, insert_character_at_start_of_text_shifts_all_remaining_text_to_the_right)
{
	std::string ncursesplusText{"cursesplus"};
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 10; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), ::ncursesplusText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	for (auto i{1u}; i <= 10; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, insert_character_in_middle_of_text_shifts_all_remaining_text_to_the_right)
{
	std::string ncursesplusText{"ncurssplus"};
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 5; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'e'});

	ASSERT_EQ(m_textBoxWidget.getText(), ::ncursesplusText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 6_x}));

	for (auto i{1u}; i <= 5; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 11_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, insert_character_at_start_of_line_shifts_last_overflowing_character_down)
{
	std::string ncursesplusLongText{"cursesplus-ncurses"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncurses");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	for (auto i{1u}; i <= 18; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 1_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_character_at_start_of_line_shifts_last_overflowing_character_down_and_next_line_text_to_the_right)
{
	std::string ncursesplusLongText{"cursesplus-ncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 4; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), ::ncursesplusLongText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	for (auto i{1u}; i <= 22; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_character_in_middle_of_line_shifts_last_overflowing_character_down_and_next_line_text_to_the_right)
{
	std::string ncursesplusLongText{"ncursesplusncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	for (auto i{1u}; i <= 7; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'-'});

	ASSERT_EQ(m_textBoxWidget.getText(), ::ncursesplusLongText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 12_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_character_at_end_of_line_shifts_last_overflowing_character_down_and_next_line_text_to_the_right)
{
	std::string ncursesplusLongText{"ncursesplus-ncurssplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	for (auto i{1u}; i <= 13; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'e'});

	ASSERT_EQ(m_textBoxWidget.getText(), ::ncursesplusLongText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	for (auto i{1u}; i <= 5; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, inserting_character_at_newline_shifts_newline_one_place_to_the_right)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), "n\n");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_character_at_start_of_line_with_newline_at_end_shifts_the_overflowing_newline_down_and_the_next_line_down)
{
	std::string ncursesplusLongText{"cursesplus-ncurse"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: "splus")
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});

	for (auto i{1u}; i <= 5; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncurse\nsplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 0_x}));

	for (auto i{1u}; i <= 5; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 5_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_character_at_end_of_line_on_a_newline_shifts_the_overflowing_newline_down_and_the_next_line_down)
{
	std::string ncursesplusLongText{"ncursesplus-ncurs"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: "splus")
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 6; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'e'});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncurse\nsplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 0_x}));

	for (auto i{1u}; i <= 5; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{2_y, 5_x}));
}

TEST_F(
	NonScrollableTextBoxWidgetTestFixture,
	insert_multiple_characters_before_newline_at_end_of_line_should_move_remaining_text_on_first_line_to_second_line_with_the_newline_at_end)
{
	std::string ncursesplusLongText{"ncursesplus-splus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-splus\nncursesplus");

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-nsplus\nncursesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 13_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'c'});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 14_x}));
	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncsplus\nncursesplus");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 continuous_character_insertion_at_end_of_line_properly_moves_remaining_text_on_next_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	for (auto i{1u}; i <= 6; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 17_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'c'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'u'});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncursncuesplus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 2_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_newline_character_at_start_of_text_shifts_existing_of_text_down_one_line)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'c'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), "\nnc");
	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{1_y, 1_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_newline_character_at_start_then_deleting_it_and_sending_down_arrow_does_not_move_cursor)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 0_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_RIGHT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), (widget::Point{0_y, 1_x}));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_newline_character_at_middle_of_full_line_text_shifts_rest_of_text_to_the_start_of_next_line)
{
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_UP});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncurs\nesplus-ncursesplus");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, del_at_end_of_text_does_not_remove_characters)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusText);
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, insert_new_line_brings_cursor_to_start_of_next_line)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplu\ns");
	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(1_y, 0_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, left_arrow_at_start_of_text_does_not_move_cursor)
{
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'n'});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(0_y, 0_x));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(0_y, 0_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_from_middle_of_text_all_the_way_does_not_delete_remaining_text_after_cursor)
{
	for (const auto& ch: ncursesplusText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});
	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_LEFT});

	for (auto i{1u}; i <= 11; i++)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), "lus");
	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(0_y, 0_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 insert_characters_after_end_line_does_not_add_any_text_and_keeps_cursor_at_same_position)
{
	std::string ncursesplusLongText{"ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncurse"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 17_x));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{'s'});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusLongText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 17_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, down_arrow_at_end_line_keeps_cursor_at_same_position)
{
	std::string ncursesplusLongText{"ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncurse"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 17_x));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DOWN});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 17_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, del_at_end_line_end_col_removes_last_character)
{
	std::string ncursesplusLongText{"ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncurse"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 17_x));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_DC});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncurs");
	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 17_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture,
		 backspace_at_end_line_end_col_removes_previous_character_and_moves_cursor_one_position_to_the_left)
{
	std::string ncursesplusLongText{"ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncurse"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 17_x));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_BACKSPACE});

	ASSERT_EQ(m_textBoxWidget.getText(), "ncursesplus-ncursesplus-ncursesplus-ncursesplus-ncure");
	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 16_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, enter_key_at_end_line_does_not_add_newline_character)
{
	std::string ncursesplusLongText{"ncursesplus-ncursesplus-ncursesplus-ncursesplus"};
	for (const auto& ch: ncursesplusLongText)
		m_textBoxWidget.handleKeyEvent(event::KeyEvent{ch});

	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 11_x));

	m_textBoxWidget.handleKeyEvent(event::KeyEvent{KEY_ENTER});

	ASSERT_EQ(m_textBoxWidget.getText(), ncursesplusLongText);
	ASSERT_EQ(m_textBoxWidget.getCursor(), widget::Point(2_y, 11_x));
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, text_box_manipulator_with_char_replacement_displays_text_replacement)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus"};

	widget::TextBoxProperties textBoxProperties;
	textBoxProperties.setReplacementChar('*');

	widget::TextManipulator textManipulator{widget::Size{3_h, 18_w}, std::move(textBoxProperties)};
	textManipulator.setCursorPos(widget::Point{0_y, 0_x});

	for (auto ch: ncursesplusLongText)
	{
		textManipulator.addCh(ch);
		textManipulator.moveCursorPos(widget::RightPos{});
	}

	ASSERT_EQ(textManipulator.getDisplayedText(), "***********************************");
}

TEST_F(NonScrollableTextBoxWidgetTestFixture, text_manipulator_with_max_length_of_10_does_not_exceed_10_characters)
{
	std::string ncursesplusLongText{"ncursesplus\nncursesplus-ncursesplus"};

	widget::TextBoxProperties textBoxProperties;
	textBoxProperties.setMaxLength(10);

	widget::TextManipulator textManipulator{widget::Size{3_h, 18_w}, std::move(textBoxProperties)};
	textManipulator.setCursorPos(widget::Point{0_y, 0_x});

	for (auto ch: ncursesplusLongText)
	{
		textManipulator.addCh(ch);
		textManipulator.moveCursorPos(widget::RightPos{});
	}

	ASSERT_EQ(textManipulator.getDisplayedText(), "ncursesplu");
}
