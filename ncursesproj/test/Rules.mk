sp := $(sp).x
dirstack_$(sp) := $(d)
d := $(dir)

SRCS_$(d) := $(wildcard $(d)/$(SRC_DIR)/*.cpp)

include common/release/Artifacts.mk
include common/debug/Artifacts.mk

GOOGLETEST_PATH := thirdparty/googletest
GOOGLETEST_INC_DIR := $(GOOGLETEST_PATH)/googletest/include
GOOGLEMOCK_INC_DIR := $(GOOGLETEST_PATH)/googlemock/include
GOOGLETEST_LIB_DIR := $(GOOGLETEST_PATH)/build/lib
GOOGLETEST_LIB := $(GOOGLETEST_LIB_DIR)/libgtest.a

$(GOOGLETEST_LIB):
	$(info [ cmake ] Generating $(GOOGLETEST_PATH) build files)
	@cmake -S $(GOOGLETEST_PATH) -B $(GOOGLETEST_PATH)/build > /dev/null
	$(info [ make ] Building $(GOOGLETEST_PATH))
	@make -C $(GOOGLETEST_PATH)/build > /dev/null

TGT_TEST_RELEASE_$(d) := $(d)/$(RELEASE_DIR)/$(BIN_DIR)/nptest
$(TGT_TEST_RELEASE_$(d)): CPPFLAGS := -I$(d)/$(INC_DIR) -Incursesproj/$(INC_DIR) -I$(GOOGLETEST_INC_DIR) -I$(GOOGLEMOCK_INC_DIR)
$(TGT_TEST_RELEASE_$(d)): LDFLAGS := -Lncursesproj/$(RELEASE_DIR)/$(LIB_DIR) -L$(GOOGLETEST_LIB_DIR)
$(TGT_TEST_RELEASE_$(d)): LDLIBS := -lncursesw -lncursesplus -lgtest -lgmock
$(TGT_TEST_RELEASE_$(d)): CXXFLAGS += -O3

$(TGT_TEST_RELEASE_$(d)): ncursesproj/$(RELEASE_DIR)/$(LIB_DIR)/libncursesplus.a $(GOOGLETEST_LIB)

TGTS_TEST_RELEASE := $(TGTS_TEST_RELEASE) $(TGT_TEST_RELEASE_$(d))

TGT_TEST_DEBUG_$(d) := $(d)/$(DEBUG_DIR)/$(BIN_DIR)/nptest
$(TGT_TEST_DEBUG_$(d)): CPPFLAGS := -I$(d)/$(INC_DIR) -Incursesproj/$(INC_DIR) -I$(GOOGLETEST_INC_DIR) -I$(GOOGLEMOCK_INC_DIR)
$(TGT_TEST_DEBUG_$(d)): LDFLAGS := -Lncursesproj/$(DEBUG_DIR)/$(LIB_DIR) -L$(GOOGLETEST_LIB_DIR)
$(TGT_TEST_DEBUG_$(d)): LDLIBS := -lncursesw -lncursesplus -lgtest -lgmock
$(TGT_TEST_DEBUG_$(d)): CXXFLAGS += -g

$(TGT_TEST_DEBUG_$(d)): ncursesproj/$(DEBUG_DIR)/$(LIB_DIR)/libncursesplus.a $(GOOGLETEST_LIB)

TGTS_TEST_DEBUG := $(TGTS_TEST_DEBUG) $(TGT_TEST_DEBUG_$(d))

include common/release/BinRules.mk
include common/debug/BinRules.mk

CLEAN := $(CLEAN) $(d)/$(BUILD_DIR)

d := $(dirstack_$(sp))
sp := $(basename $(sp))
