#ifndef TESTHELPERS_HPP
#define TESTHELPERS_HPP
#include "LayoutStrategy.hpp"
#include "Widget.hpp"
#include "WidgetBuilder.hpp"
#include "WidgetBuilderCommon.hpp"

namespace ncursesplus
{
namespace test::helpers
{
struct DummyLayoutConstraint
{};

class DummyLayoutStrategy;
}

template<>
struct layout::LayoutStrategyTraits<test::helpers::DummyLayoutStrategy>
{
	using LayoutConstraintType = test::helpers::DummyLayoutConstraint;
};

namespace test::helpers
{
struct StubLayoutConstraint
{
	widget::Size m_size;
	widget::Point m_point;
};

class StubLayoutStrategy;
}

template<>
struct layout::LayoutStrategyTraits<test::helpers::StubLayoutStrategy>
{
	using LayoutConstraintType = test::helpers::StubLayoutConstraint;
};

namespace test::helpers
{
class DummyWidget final : public widget::Widget
{
public:
	DummyWidget(const std::string&, widget::Size, widget::Point);
	widget::Point getCursor() const override;
private:
	void handleKeyEventImpl(event::KeyEvent) override;
	void moveCursorImpl() const override;
	void drawImpl() const override;
};

class DummyWidgetBuilder final : public widget::builder::WidgetBuilder<DummyWidgetBuilder>
{
public:
	DummyWidgetBuilder(std::string);
private:
	std::unique_ptr<widget::Widget> buildImpl() override;
};

class DummyLayoutStrategy : public layout::LayoutStrategy<DummyLayoutStrategy>
{
public:
	consteval DummyLayoutStrategy();
private:
	template<unsigned int NumOfConstraints>
	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
	layoutWidgetsImpl(const LayoutConstraintType (&)[NumOfConstraints]) const;
	template<unsigned int NumOfConstraints>
	void validate(const LayoutConstraintType (&)[NumOfConstraints]) const;
private:
	friend layout::LayoutStrategy<DummyLayoutStrategy>;
};

consteval DummyLayoutStrategy::DummyLayoutStrategy()
	 : layout::LayoutStrategy<DummyLayoutStrategy>{layout::Rows<1u>{}, layout::Cols<1u>{}}
{}


template<unsigned int NumOfConstraints>
std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
DummyLayoutStrategy::layoutWidgetsImpl(const LayoutConstraintType (&)[NumOfConstraints]) const
{
	return {};
}

template<unsigned int NumOfConstraints>
void DummyLayoutStrategy::validate(const LayoutConstraintType (&)[NumOfConstraints]) const
{}

class StubLayoutStrategy : public layout::LayoutStrategy<StubLayoutStrategy>
{
public:
	consteval StubLayoutStrategy();
private:
	template<unsigned int NumOfConstraints>
	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
	layoutWidgetsImpl(const LayoutConstraintType (&)[NumOfConstraints]) const;
	template<unsigned int NumOfConstraints>
	void validate(const LayoutConstraintType (&)[NumOfConstraints]) const;
private:
	friend layout::LayoutStrategy<StubLayoutStrategy>;
};

consteval StubLayoutStrategy::StubLayoutStrategy()
	 : layout::LayoutStrategy<StubLayoutStrategy>{layout::Rows<1u>{}, layout::Cols<1u>{}}
{}

template<unsigned int NumOfConstraints>
std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints>
StubLayoutStrategy::layoutWidgetsImpl(const LayoutConstraintType (&layoutConstraints)[NumOfConstraints]) const
{
	std::array<std::pair<widget::Size, widget::Point>, NumOfConstraints> widgetLayouts;
	unsigned int i{0u};
	for (const auto& layoutConstraint: layoutConstraints)
	{
		auto widgetSize{layoutConstraint.m_size};
		auto widgetPoint{layoutConstraint.m_point};
		widgetLayouts[i] = {widgetSize, widgetPoint};
		i++;
	}
	return widgetLayouts;
}

template<unsigned int NumOfConstraints>
void StubLayoutStrategy::validate(const LayoutConstraintType (&)[NumOfConstraints]) const
{}
}
}
#endif
