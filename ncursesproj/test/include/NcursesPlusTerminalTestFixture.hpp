#ifndef NCURSESPLUSTERMINALTESTFIXTURE_HPP
#define NCURSESPLUSTERMINALTESTFIXTURE_HPP
#include <gtest/gtest.h>
#include <ncurses.h>

namespace ncursesplus::test
{
class NcursesPlusTerminalTestFixture : public ::testing::Test
{
protected:
	NcursesPlusTerminalTestFixture(int, int);
	~NcursesPlusTerminalTestFixture();
private:
	FILE* fileHandle;
	SCREEN* screen;
};
}
#endif
