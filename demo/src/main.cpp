#include "CustomGridLayoutStrategy.hpp"
#include "EventEngine.hpp"
#include "NeighborWidgetsMediator.hpp"
#include "Session.hpp"
#include "SideBarWidgetBuilder.hpp"
#include "TextBoxWidgetBuilder.hpp"
#include "WidgetCompositeBuilder.hpp"

#include <ncurses.h>

using namespace ncursesplus;
using namespace widget::literals;
using namespace std::string_literals;

int main()
{
	auto session{Session{}.withColor()};
	auto neighborWidgetsMediator{std::make_shared<widget::NeighborWidgetsMediator>()};
	auto maxLines{static_cast<unsigned int>(LINES)};
	auto maxCols{static_cast<unsigned int>(COLS)};
	auto widgetComposite{
		widget::builder::WidgetCompositeBuilder{"WidgetComposite", widget::builder::NumOfWidgets<4>{},
															 layout::CustomGridLayoutStrategy{layout::Rows<5u>{}, layout::Cols<5u>{}}}
			.withSize(widget::Size{widget::Height{maxLines}, widget::Width{maxCols}})
			.withPoint(widget::Point{0_y, 0_x})
			.add(widget::builder::SideBarWidgetBuilder<3, 20>{"SideBarWidget"}
					  .withMediator(neighborWidgetsMediator)
					  .withMenuEntries("MenuEntry1"s, "MenuEntry2"s, "MenuEntry3"s),
				  layout::CustomGridLayoutConstraint{0, 3, 0, 1})
			.add(widget::builder::TextBoxWidgetBuilder{"TextBoxWidget1"}.withMediator(neighborWidgetsMediator).asActive(),
				  layout::CustomGridLayoutConstraint{0, 2, 1, 2})
			.add(widget::builder::TextBoxWidgetBuilder{"TextBoxWidget2"}.withMediator(neighborWidgetsMediator),
				  layout::CustomGridLayoutConstraint{0, 2, 2, 3})
			.add(widget::builder::TextBoxWidgetBuilder{"TextBoxWidget3"}.withMediator(neighborWidgetsMediator),
				  layout::CustomGridLayoutConstraint{0, 2, 3, 4})
			.build()};
	widgetComposite->draw();
	event::EventEngine eventEngine;
	eventEngine.launch(*widgetComposite);
}
