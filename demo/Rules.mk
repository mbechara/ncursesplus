sp := $(sp).x
dirstack_$(sp) := $(d)
d := $(dir)

SRCS_$(d) := $(wildcard $(d)/$(SRC_DIR)/*.cpp)

include common/release/Artifacts.mk
include common/debug/Artifacts.mk

TGT_DEMO_RELEASE := $(d)/$(RELEASE_DIR)/$(BIN_DIR)/demo
$(TGT_DEMO_RELEASE): CPPFLAGS := -Incursesproj/$(INC_DIR)
$(TGT_DEMO_RELEASE): LDFLAGS := -Lncursesproj/$(RELEASE_DIR)/$(LIB_DIR)
$(TGT_DEMO_RELEASE): LDLIBS := -lncursesw -lncursesplus
$(TGT_DEMO_RELEASE): CXXFLAGS += -O3

$(TGT_DEMO_RELEASE): ncursesproj/$(RELEASE_DIR)/$(LIB_DIR)/libncursesplus.a

TGT_DEMO_DEBUG := $(d)/$(DEBUG_DIR)/$(BIN_DIR)/demo
$(TGT_DEMO_DEBUG): CPPFLAGS := -Incursesproj/$(INC_DIR)
$(TGT_DEMO_DEBUG): LDFLAGS := -Lncursesproj/$(DEBUG_DIR)/$(LIB_DIR)
$(TGT_DEMO_DEBUG): LDLIBS := -lncursesw -lncursesplus
$(TGT_DEMO_DEBUG): CXXFLAGS += -g

$(TGT_DEMO_DEBUG): ncursesproj/$(DEBUG_DIR)/$(LIB_DIR)/libncursesplus.a

include common/release/BinRules.mk
include common/debug/BinRules.mk

CLEAN := $(CLEAN) $(d)/$(BUILD_DIR)

d := $(dirstack_$(sp))
sp := $(basename $(sp))
