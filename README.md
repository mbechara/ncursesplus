# NcursesPlus
This is a work in progress modern C++ convenience wrapper around the ncurses library. This project aims to provide an easier and quicker way to build a TUI using modern C++.

## Getting Started
### Dependencies
* Linux based OS.
* Git
* Make
* ncurses library.
* GoogleTest library.

### Building
#### Install
To clone **GoogleTest** as a submodule you can use the following:
```
$ make install
```
#### Release
To build **NcursesPlus** you can use:
```
$ make
```
or
```
$ make release
```
Output files can be found in `ncursesproj/build/release`

To build the **unit tests** you can use:
```
$ make test
```
or
```
$ make test-release
```
Output files can be found in `ncursesproj/test/build/release`

To build the **demo** you can use:
```
$ make demo
```
or
```
$ make demo-release
```
Output files can be found in `demo/build/release`

#### Debug
To build **NcursesPlus** you can use:
```
$ make debug
```
Output files can be found in `ncursesproj/build/debug`

To build the **unit tests** you can use:
```
$ make test-debug
```
Output files can be found in `ncursesproj/test/build/debug`

To build the **demo** you can use:
```
$ make demo-debug
```
Output files can be found in `demo/build/debug`

#### Clean
To clean the output files you can use the following command:
```
$ make clean
```

## Contributing
Merge requests are welcome. For features or impactful refactorings please open an issue first.
Please make sure to cover the new code with unit tests and/or modify the existing tests as appropriate.

## License
This project is licensed under the MIT License - see the LICENSE file for more details.
