
all: release

d := .

dir := ncursesproj
include $(dir)/Rules.mk

dir := demo
include $(dir)/Rules.mk

.PHONY: release
release: $(TGTS_LIB_RELEASE)

.PHONY: debug
debug: $(TGTS_LIB_DEBUG)

.PHONY: test
test: test-release

.PHONY: test-release
test-release: $(TGTS_TEST_RELEASE)

.PHONY: test-debug
test-debug: $(TGTS_TEST_DEBUG)

.PHONY: demo
demo: demo-release

.PHONY: demo-release
demo-release: $(TGT_DEMO_RELEASE)

.PHONY: demo-debug
demo-debug: $(TGT_DEMO_DEBUG)

.PHONY: install
install:
	git submodule update --init --recursive

.PHONY: clean
clean:
	@rm -rf $(CLEAN)
	$(foreach dir,$(CLEAN),$(info [ rm ] Removing $(dir) directory))

.SECONDARY: $(CLEAN)
