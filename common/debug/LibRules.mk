$(TGT_LIB_DEBUG_$(d)): | $(d)/$(DEBUG_DIR)/$(LIB_DIR)

$(d)/$(DEBUG_DIR)/$(LIB_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

$(TGT_LIB_DEBUG_$(d)): $(OBJS_DEBUG_$(d))
	$(info [ $(AR) ] Creating static lib $@)
	@$(ARCH)

include common/debug/ObjRules.mk
