$(OBJS_DEBUG_$(d)): | $(d)/$(DEBUG_DIR)/$(OBJ_DIR) $(d)/$(DEBUG_DIR)/$(OBJ_DIR)/$(DEP_DIR)

$(d)/$(DEBUG_DIR)/$(OBJ_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

$(d)/$(DEBUG_DIR)/$(OBJ_DIR)/%.o: $(d)/$(SRC_DIR)/%.cpp $(d)/$(DEBUG_DIR)/$(OBJ_DIR)/$(DEP_DIR)/%.d
	$(info [ $(CXX) ] Compiling $<)
	@$(COMP)
	@$(POSTCOMP)

$(d)/$(DEBUG_DIR)/$(OBJ_DIR)/$(DEP_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

$(DEPS_DEBUG_$(d)):

include $(DEPS_DEBUG_$(d))
