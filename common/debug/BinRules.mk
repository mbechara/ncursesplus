$(d)/$(DEBUG_DIR)/$(BIN_DIR)/%: $(OBJS_DEBUG_$(d)) | $(d)/$(DEBUG_DIR)/$(BIN_DIR)
	$(info [ linking ] Linking $@)
	@$(LINK)

$(d)/$(DEBUG_DIR)/$(BIN_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

include common/debug/ObjRules.mk
