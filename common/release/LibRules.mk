$(TGT_LIB_RELEASE_$(d)): | $(d)/$(RELEASE_DIR)/$(LIB_DIR)

$(d)/$(RELEASE_DIR)/$(LIB_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

$(TGT_LIB_RELEASE_$(d)): $(OBJS_RELEASE_$(d))
	$(info [ $(AR) ] Creating static lib $@)
	@$(ARCH)

include common/release/ObjRules.mk
