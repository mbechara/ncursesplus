$(d)/$(RELEASE_DIR)/$(BIN_DIR)/%: $(OBJS_RELEASE_$(d)) | $(d)/$(RELEASE_DIR)/$(BIN_DIR)
	$(info [ linking ] Linking $@)
	@$(LINK)

$(d)/$(RELEASE_DIR)/$(BIN_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

include common/release/ObjRules.mk
