$(OBJS_RELEASE_$(d)): | $(d)/$(RELEASE_DIR)/$(OBJ_DIR) $(d)/$(RELEASE_DIR)/$(OBJ_DIR)/$(DEP_DIR)

$(d)/$(RELEASE_DIR)/$(OBJ_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

$(d)/$(RELEASE_DIR)/$(OBJ_DIR)/%.o: $(d)/$(SRC_DIR)/%.cpp $(d)/$(RELEASE_DIR)/$(OBJ_DIR)/$(DEP_DIR)/%.d
	$(info [ $(CXX) ] Compiling $<)
	@$(COMP)
	@$(POSTCOMP)

$(d)/$(RELEASE_DIR)/$(OBJ_DIR)/$(DEP_DIR):
	$(info [ mkdir ] Creating directory $@)
	@mkdir -p $@

$(DEPS_RELEASE_$(d)):

include $(DEPS_RELEASE_$(d))
