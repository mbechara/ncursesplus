## Problem to solve

<!-- What is the problem you are trying to solve with this issue? !-->

## Proposal
### Explanation

<!-- Use this section to explain the proposed refactoring in more details. -->

### Technical details

<!-- Use this section to add any technical details that can be helpful to the refactoring. It's okay to write "Unknown" and fill this field in later. -->

## Potential risks

<!-- Use this section to add any potential risks due to the refactoring. It's okay to write "Unknown" and fill this field later. -->

/label ~refactoring
