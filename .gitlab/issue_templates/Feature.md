## Problem to solve

<!-- What is the user problem you are trying to solve with this issue? -->

## Proposal
### Explanation

<!-- Use this section to explain the feature and how it will work. -->

### Technical details

<!-- Use this section to add any technical details that can be helpful to the implementation of the feature. It's okay to write "Unknown" and fill this field later. -->

### Extensions

<!-- Use this section to add any potential failures in the behavior of the feature to be implemented, along with alternative steps to be taken. It's okay to write "Unknown" and fill this field later. -->

## Intended users

<!-- Who will use this feature? -->

/label ~feature
